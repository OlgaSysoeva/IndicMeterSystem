﻿namespace UserNotification
{
    /// <summary>
    /// Класс параметров для отправки email.
    /// </summary>
    public class SmptConfig
    {
        /// <summary>
        /// Smtp сервер.
        /// </summary>
        public string Server { get; set; }

        /// <summary>
        /// Email адрес учетной записи отправителя.
        /// </summary>
        public string SenderEmail { get; set; }

        /// <summary>
        /// Пароль учетной записи отправителя.
        /// </summary>
        public string SenderPass { get; set; }

        /// <summary>
        /// Порт подключения к серверу.
        /// </summary>
        public int Port { get; set; }

        /// <summary>
        /// Использование сертификата защиты.
        /// </summary>
        public bool SslEnabled { get; set; }
    }
}