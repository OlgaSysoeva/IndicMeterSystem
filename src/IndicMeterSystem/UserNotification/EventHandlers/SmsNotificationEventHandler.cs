﻿using BusinesEvent;

using Infrastructure.EventBus.EventBusInfrastructure.Abstraction;

using Microsoft.Extensions.Logging;

using System;
using System.Threading.Tasks;

namespace UserNotification
{
    public class SmsNotificationEventHandler :
        IIntegrationEventHandler<UserSmsNotifEvent>
    {
        private readonly ILogger<SmsNotificationEventHandler> _logger;

        public SmsNotificationEventHandler(
            ILogger<SmsNotificationEventHandler> logger)
        {
            _logger = logger;
        }

        public async Task Handle(UserSmsNotifEvent @event)
        {
            if (DateTime.Today < @event.EventDateTo)
            {
                await SendSmsAsync(@event.PhoneNumber, @event.Body);
            }
        }

        /// <summary>
        /// Отправляет смс сообщения.
        /// </summary>
        /// <param name="phoneNumber">Номер телефрна получателя.</param>
        /// <param name="message">Текст сообщения.</param>
        /// <returns></returns>
        private async Task SendSmsAsync(string phoneNumber, string message)
        {
            try
            {
                _logger.LogDebug("\nОтправлено оповещение по sms: "
                    + $" номер получателя '{phoneNumber}';"
                    + $" текст '{message}'.");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Ошибка отправки sms пользователю {phoneNumber}.");
            }
        }
    }
}
