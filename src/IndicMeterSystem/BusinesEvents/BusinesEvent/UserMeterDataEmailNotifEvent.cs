﻿using System;

namespace BusinesEvent
{
    public class UserMeterDataEmailNotifEvent : UserEmailNotifEvent
    {
        private bool IsDisposed = false;

        public UserMeterDataEmailNotifEvent(string email, DateTime eventDateTo,
            string address)
            :base(email, eventDateTo)
        {
            Title = "Время вносить показания";
            Body = "Уважаемы пользователь!"
                    + Environment.NewLine
                    + $"По адресу \"{address}\" отсутствуют показания на текущий месяц.";
        }

        protected override void Dispose(bool disposing)
        {
            if (IsDisposed) return;

            if (disposing)
            {
            }

            IsDisposed = true;
            base.Dispose(disposing);
        }
    }
}
