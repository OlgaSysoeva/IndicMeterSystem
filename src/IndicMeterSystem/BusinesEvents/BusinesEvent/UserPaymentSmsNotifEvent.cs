﻿using System;

namespace BusinesEvent
{
    public class UserPaymentSmsNotifEvent : UserSmsNotifEvent
    {
        private bool IsDisposed = false;

        public UserPaymentSmsNotifEvent(string phoneNumber, DateTime eventDateTo,
            string address, DateTime? date)
            : base(phoneNumber, eventDateTo)
        {
            Body = $"По адресу \"{address}\" имеются неоплаченные показания, " +
                    $"внесенные датой: {date}.";
        }

        protected override void Dispose(bool disposing)
        {
            if (IsDisposed) return;

            if (disposing)
            {
            }

            IsDisposed = true;
            base.Dispose(disposing);
        }
    }
}
