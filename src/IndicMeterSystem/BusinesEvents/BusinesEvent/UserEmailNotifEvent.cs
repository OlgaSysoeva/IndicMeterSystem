﻿using Infrastructure.EventBus.EventBusInfrastructure.Event;

using System;

namespace BusinesEvent
{
    public abstract class UserEmailNotifEvent : IntegrationEvent, IDisposable
    {
        /// <summary>
        /// Адрес получателя.
        /// </summary>
        public string Email { get; }

        /// <summary>
        /// Заголовок сообщения.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Тело сообщения.
        /// </summary>
        public string Body { get; set; }

        /// <summary>
        /// Дата, до которой событие актуально.
        /// </summary>
        public DateTime EventDateTo { get; }

        private bool disposed = false;

        protected UserEmailNotifEvent(string email, DateTime eventDateTo)
        {
            EventDateTo = eventDateTo;
            Email = email;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                }
                disposed = true;
            }
        }
    }
}
