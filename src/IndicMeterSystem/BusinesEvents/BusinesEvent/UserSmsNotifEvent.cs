﻿using Infrastructure.EventBus.EventBusInfrastructure.Event;

using System;

namespace BusinesEvent
{
    public abstract class UserSmsNotifEvent : IntegrationEvent, IDisposable
    {
        /// <summary>
        /// Номер телефона получателя.
        /// </summary>
        public string PhoneNumber { get; }

        /// <summary>
        /// Тело сообщения.
        /// </summary>
        public string Body { get; set; }

        /// <summary>
        /// Дата, до которой событие актуально.
        /// </summary>
        public DateTime EventDateTo { get; }

        private bool disposed = false;

        protected UserSmsNotifEvent(string phoneNumber, DateTime eventDateTo)
        {
            EventDateTo = eventDateTo;
            PhoneNumber = phoneNumber;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                }
                disposed = true;
            }
        }
    }
}
