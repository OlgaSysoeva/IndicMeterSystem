﻿using IndicMeterSystem.Data;
using IndicMeterSystem.Data.EF;
using IndicMeterSystem.Data.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace ClearAndTestInitDatabase
{
    class DbInitializer
    {
        private readonly string connectionString;

        private Tariff tariffER;
        private Tariff tariffEB;
        private Tariff tariffCW;
        private Tariff tariffHW;

        public DbInitializer(string connectionString)
            => this.connectionString = connectionString;
        
        public void InitDb()
        {
            if (!AreYouSure("База данных будет очищена")) return;

            try
            {
                SemaphoreSetting.MaximumThreadsToAccessDatabase = 1;
                var optionsBuilder = new DbContextOptionsBuilder<ApplicationContext>();
                optionsBuilder.UseNpgsql(connectionString);
                using (ApplicationContext db = new ApplicationContext(optionsBuilder.Options))
                {
                    DeleteAll(db);
                    if (!AreYouSure("База данных будет заполнена тестовыми данными")) return;
                    AddTariffs(db);
                    AddUsers(db);
                    db.SaveChanges();
                }
                Console.WriteLine("Инициализация выполнена успешно");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Ошибка инициализации БД:\n" + ex.InnerException?.Message ?? ex.Message);
            }
        }

        private bool AreYouSure(string msg)
        {
            Console.WriteLine(msg + "\nВведите 'Y' для продолжения");
            return Console.ReadLine().ToLower() == "y";
        }

        private static void DeleteAll(ApplicationContext db)
        {
            var users = db.Users.ToList();
            var tariffs = db.Tariffs.ToList();
            db.Users.RemoveRange(users);
            db.Tariffs.RemoveRange(tariffs);
            db.SaveChanges();
        }

        private void AddTariffs(ApplicationContext db)
        {
            tariffER = new Tariff
            {
                TariffTypeId = 3,
                Title = "Тариф за электричество R",
                Deleted = false,
                Unit = "кВт*ч",
                SubTariffs = new SubTariff[]
                {
                    new SubTariff
                    {
                        Number = 1,
                        AddDate = new DateTime(2020, 01, 01),
                        Price = 5
                    },
                    new SubTariff
                    {
                        Number = 2,
                        AddDate = new DateTime(2020, 01, 01),
                        Price = 4
                    }
                }
            };
            tariffEB = new Tariff
            {
                TariffTypeId = 2,
                Title = "Тариф за электричество B",
                Deleted = false,
                Unit = "кВт*ч",
                SubTariffs = new SubTariff[]
                {
                    new SubTariff
                    {
                        Number = 1,
                        AddDate = new DateTime(2020, 01, 01),
                        Price = 1
                    },
                    new SubTariff
                    {
                        Number = 1,
                        AddDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 01),  //вначале этого месяца тариф поменялся
                        Price = 2
                    }
                }
            };
            tariffCW = new Tariff
            {
                TariffTypeId = 2,
                Title = "Тариф за холодную воду",
                Deleted = false,
                Unit = "м3",
                SubTariffs = new SubTariff[]
                {
                    new SubTariff
                    {
                        Number = 1,
                        AddDate = new DateTime(2020, 01, 01),
                        Price = 40
                    }
                }
            };
            tariffHW = new Tariff
            {
                TariffTypeId = 2,
                Title = "Тариф за горячую воду",
                Deleted = false,
                Unit = "м3",
                SubTariffs = new SubTariff[]
                {
                    new SubTariff
                    {
                        Number = 1,
                        AddDate = new DateTime(2020, 01, 01),
                        Price = 200
                    }
                }
            };
            db.Tariffs.AddRange(tariffER, tariffEB, tariffCW, tariffHW);
        }

        private void AddUsers(ApplicationContext db)
        {
            db.Users.AddRange(new User[]
            {
                new User
                {
                    UserName = "alex",
                    Login = "alex",
                    Password = "alex",
                    Deleted = false,
                    Email = "alex@mail.ru",
                    PhoneNumber = "+71234567890",
                    GetEmailNotification = true,
                    GetSmsNotification = true,
                    Addresses = new Address[]
                    {
                        new Address
                        {
                            RegistrationAddress = "СПб, улица Профессора Попова, д.3, кв.33",
                            Name = "Альма-матер",
                            Deleted = false,
                            Meters = new Meter[]
                            {
                                new Meter
                                {
                                    DateFrom = new DateTime(2020, 01, 01),
                                    DateTo = new DateTime(2025, 01, 01),
                                    SerialNumber = 111124,
                                    Tariff = tariffCW,
                                    Description = "Счетчик хол.воды на Попова",
                                    MeterDatas = new MeterData[]
                                    {
                                        new MeterData
                                        {
                                            Date = DateTime.Now.AddMonths(-1),
                                            Description = "показания х/в за " + DateTime.Now.AddMonths(-1).ToString("MMMM"),
                                            MeterValues = new MeterValue[]
                                            {
                                                new MeterValue
                                                {
                                                    Number = 1,
                                                    Value = 5,
                                                    Difference = 5
                                                }
                                            },
                                            Payments = new Payment[]
                                            {
                                                new Payment
                                                {
                                                    Date = DateTime.Now.AddMonths(-1),
                                                    Amount = 200
                                                }
                                            },
                                            PaymentState = 3
                                        },
                                        new MeterData
                                        {
                                            Date = DateTime.Now,
                                            Description = "показания х/в за " + DateTime.Now.ToString("MMMM"),
                                            MeterValues = new MeterValue[]
                                            {
                                                new MeterValue
                                                {
                                                    Number = 1,
                                                    Value = 10,
                                                    Difference = 5
                                                }
                                            },
                                            Payments = new Payment[]
                                            {
                                                new Payment
                                                {
                                                    Date = DateTime.Now,
                                                    Amount = 200
                                                }
                                            },
                                            PaymentState = 3
                                        }
                                    }
                                },
                                new Meter
                                {
                                    DateFrom = new DateTime(2020, 01, 01),
                                    DateTo = new DateTime(2025, 01, 01),
                                    SerialNumber = 111125,
                                    Tariff = tariffHW,
                                    Description = "Счетчик гор.воды на Попова",
                                    MeterDatas = new MeterData[]
                                    {
                                        new MeterData
                                        {
                                            Date = DateTime.Now.AddMonths(-1),
                                            Description = "показания г/в за " + DateTime.Now.AddMonths(-1).ToString("MMMM"),
                                            MeterValues = new MeterValue[]
                                            {
                                                new MeterValue
                                                {
                                                    Number = 1,
                                                    Value = 6,
                                                    Difference = 6
                                                }
                                            },
                                            Payments = new Payment[]
                                            {
                                                new Payment
                                                {
                                                    Date = DateTime.Now.AddMonths(-1),
                                                    Amount = 1200
                                                }
                                            },
                                            PaymentState = 3
                                        },
                                        //За текущий месяц нет ни показаний, ни оплаты
                                    }
                                }
                            }
                        }
                    }
                },
                new User
                {
                    UserName = "olga",
                    Login = "olga",
                    Password = "olga",
                    Deleted = false,
                    Email = "olga@mail.ru",
                    PhoneNumber = "+71232234567",
                    GetEmailNotification = true,
                    GetSmsNotification = true,
                    Addresses = new Address[]
                    {
                        new Address
                        {
                            RegistrationAddress = "Могилев, улица Лазаренко, 73Б, кв.33",
                            Name = "Квартира на Лазаренко",
                            Deleted = false,
                            Meters = new Meter[]
                            {
                                new Meter
                                {
                                    DateFrom = new DateTime(2020, 01, 01),
                                    DateTo = new DateTime(2025, 01, 01),
                                    SerialNumber = 111126,
                                    Tariff = tariffEB,
                                    Description = "Счетчик электричества на Лазоренко",
                                    MeterDatas = new MeterData[]
                                    {
                                        new MeterData
                                        {
                                            Date = DateTime.Now.AddMonths(-1),
                                            Description = "эл-во за " + DateTime.Now.AddMonths(-1).ToString("MMMM"),
                                            MeterValues = new MeterValue[]
                                            {
                                                new MeterValue
                                                {
                                                    Number = 1,
                                                    Value = 200,
                                                    Difference = 200
                                                }
                                            },
                                            Payments = new Payment[]
                                            {
                                                new Payment
                                                {
                                                    Date = DateTime.Now.AddMonths(-1),
                                                    Amount = 200
                                                }
                                            },
                                            PaymentState = 3
                                        },
                                        new MeterData
                                        {
                                            Date = DateTime.Now,
                                            Description = "эл-во за " + DateTime.Now.ToString("MMMM"),
                                            MeterValues = new MeterValue[]
                                            {
                                                new MeterValue
                                                {
                                                    Number = 1,
                                                    Value = 500,
                                                    Difference = 300
                                                }
                                            },
                                            PaymentState = 1
                                            //По счетчику 3 за ноябрь нет платежа
                                        }
                                    }
                                }
                            }
                        }
                    }
                },
                new User
                {
                    UserName = "serg",
                    Login = "serg",
                    Password = "serg",
                    Deleted = false,
                    Email = "serg@mail.ru",
                    PhoneNumber = "+79993234567",
                    GetEmailNotification = true,
                    GetSmsNotification = true,
                    Addresses = new Address[]
                    {
                        new Address
                        {
                            RegistrationAddress = "Москва, 3-я улица Строителей, д.3, кв.12",
                            Name = "Квартира на Строителей",
                            Deleted = false,
                            Meters = new Meter[]
                            {
                                new Meter
                                {
                                    DateFrom = new DateTime(2020, 01, 01),
                                    DateTo = new DateTime(2025, 01, 01),
                                    SerialNumber = 111127,
                                    Tariff = tariffER,
                                    Description = "Счетчик электричества на Строителей",
                                    MeterDatas = new MeterData[]
                                    {
                                        new MeterData
                                        {
                                            Date = DateTime.Now.AddMonths(-1),
                                            Description = "электричество за " + DateTime.Now.AddMonths(-1).ToString("MMMM"),
                                            MeterValues = new MeterValue[]
                                            {
                                                new MeterValue
                                                {
                                                    Number = 1,
                                                    Value = 200,
                                                    Difference = 200
                                                },
                                                new MeterValue
                                                {
                                                    Number = 2,
                                                    Value = 100,
                                                    Difference = 100
                                                }
                                            },   
                                            Payments = new Payment[]
                                            {
                                                new Payment
                                                {
                                                    Date = DateTime.Now.AddMonths(-1),
                                                    Amount = 1400
                                                }
                                            },
                                            PaymentState = 3
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            });
        }
    }
}
