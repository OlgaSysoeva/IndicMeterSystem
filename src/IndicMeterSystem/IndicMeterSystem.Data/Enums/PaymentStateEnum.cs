﻿namespace IndicMeterSystem.Data.Enums
{
    /// <summary>
    /// Статусы оплаты показаний.
    /// </summary>
    public enum PaymentStateEnum
    {
        /// <summary>
        /// Неоплаченные показания.
        /// </summary>
        None = 1,

        /// <summary>
        /// Частично оплаченные показания.
        /// </summary>
        Partial = 2,

        /// <summary>
        /// Подностью оплаченные показания.
        /// </summary>
        Full = 3
    }
}
