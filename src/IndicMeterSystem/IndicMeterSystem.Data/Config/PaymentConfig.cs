﻿using IndicMeterSystem.Data.Entities;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IndicMeterSystem.Data.Config
{
    class PaymentConfig : IEntityTypeConfiguration<Payment>
    {
        public void Configure(EntityTypeBuilder<Payment> builder)
        {
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Date)
                .IsRequired();

            builder.Property(x => x.Amount)
                .IsRequired();

            builder.Property(x => x.MeterDataId)
                .IsRequired();


            builder.HasOne(x => x.MeterData)
               .WithMany(y => y.Payments)
               .HasForeignKey(x => x.MeterDataId);
        }
    }
}
