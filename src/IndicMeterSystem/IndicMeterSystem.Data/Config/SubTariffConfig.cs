﻿using IndicMeterSystem.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IndicMeterSystem.Data.Config
{
    class SubTariffConfig : IEntityTypeConfiguration<SubTariff>
    {
        public void Configure(EntityTypeBuilder<SubTariff> builder)
        {
            builder.HasKey(x => x.Id);

            builder.Property(x => x.TariffId)
                .IsRequired();

            builder.Property(x => x.Number)
                .IsRequired();

            builder.Property(x => x.Price)
                .IsRequired();

            builder.Property(x => x.AddDate)
                .IsRequired();

            builder.HasOne(x => x.Tariff)
                .WithMany(y => y.SubTariffs)
                .HasForeignKey(x => x.TariffId);
        }
    }
}
