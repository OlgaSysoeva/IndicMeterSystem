﻿using IndicMeterSystem.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IndicMeterSystem.Data.Config
{
    class UserConfig : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.HasKey(x => x.Id);

            builder.Property(x => x.UserName)
                .IsRequired();

            builder.Property(x => x.Login)
                .IsRequired();

            builder.Property(x => x.Password)
                .IsRequired();

            builder.Property(x => x.Email)
                .IsRequired();

            builder.Property(x => x.Deleted)
                .HasDefaultValue(false)
                .IsRequired();

            builder.Property(x => x.GetEmailNotification)
                .HasDefaultValue(true)
                .IsRequired();

            builder.Property(x => x.GetSmsNotification)
                .HasDefaultValue(false)
                .IsRequired();
        }
    }
}
