﻿using IndicMeterSystem.Data.Entities;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IndicMeterSystem.Data.Config
{
    class MeterDataConfig : IEntityTypeConfiguration<MeterData>
    {
        public void Configure(EntityTypeBuilder<MeterData> builder)
        {
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Date)
                .IsRequired();

            builder.Property(x => x.Description)
                .HasMaxLength(1000);

            builder.Property(x => x.MeterId)
                .IsRequired();

            builder.Property(x => x.PaymentState)
                .IsRequired()
                .HasDefaultValue(1);

            builder.HasOne(x => x.Meter)
               .WithMany(y => y.MeterDatas)
               .HasForeignKey(x => x.MeterId);
        }
    }
}
