﻿using System.Reflection;
using System.Threading.Tasks;

using IndicMeterSystem.Data.Entities;

using Microsoft.EntityFrameworkCore;

namespace IndicMeterSystem.Data.EF
{
    public class ApplicationContext : DbContext
    {
        public DbSet<MeterData> MeterDatas { get; set; }
        public DbSet<Meter> Meters { get; set; }
        public DbSet<Payment> Payments { get; set; }
        public DbSet<SubTariff> SubTariffs { get; set; }
        public DbSet<Tariff> Tariffs { get; set; }
        public DbSet<TariffType> TariffTypes { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Address> Adresses { get; set; }
        public DbSet<MeterValue> MeterValues { get; set; }

        static ApplicationContext()
        {
            SemaphoreSetting.Init();
        }

        public ApplicationContext(DbContextOptions<ApplicationContext> options)
            : base(options)
        {
            SemaphoreSetting.Pool.Wait();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
            base.OnModelCreating(modelBuilder);
        }

        public override ValueTask DisposeAsync()
        {
            // Освобождаем Semaphore.
            SemaphoreSetting.Pool.Release();

            return base.DisposeAsync();
        }
    }
}
