﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace IndicMeterSystem.Data.Interfaces
{
    public interface IRepository<T> where T : class
    {
        Task<IEnumerable<T>> GetAllAsync();

        Task<T> GetAsync(int id);

        Task<IEnumerable<T>> GetWithIncludeOnConditionAsync(string predicateIncl, 
            Expression<Func<T, bool>> predicateCond);

        Task<IEnumerable<T>> GetWithIncludeOnConditionAsync(string predicateIncl1,
            string predicateIncl2, Expression<Func<T, bool>> predicateCond);

        Task<IEnumerable<T>> GetOnConditionAsync(Expression<Func<T, bool>> predicate);

        Task<T> AddAsync(T item);

        Task<bool> AnyAsync(Expression<Func<T, bool>> predicate);

        Task UpdateAsync(T item);

        Task DeleteAsync(int id);
    }
}
