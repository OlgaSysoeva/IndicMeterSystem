﻿using System;
using System.Collections.Generic;

namespace IndicMeterSystem.Data.Entities
{
    public class Meter
    {
        /// <summary>
        /// Id счетчика
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Серийный номер
        /// </summary>
        public int SerialNumber { get; set; }

        /// <summary>
        /// Id адреса
        /// </summary>
        public int AddressId { get; set; }

        /// <summary>
        /// Описание счетчика
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Дата установки
        /// </summary>
        public DateTime DateFrom { get; set; }

        /// <summary>
        /// Дата замены
        /// </summary>
        public DateTime? DateTo { get; set; }

        /// <summary>
        /// Id тарифного плана
        /// </summary>
        public int TariffId { get; set; }

        /// <summary>
        /// Признак актуальности счетчика.
        /// </summary>
        public bool Deleted { get; set; }

        /// <summary>
        /// Связанный адрес
        /// </summary>
        public Address Address { get; set; }

        // <summary>
        /// Связанный тариф
        /// </summary>
        public Tariff Tariff { get; set; }

        /// <summary>
        /// Список показаний счетчиков
        /// </summary>
        public IEnumerable<MeterData> MeterDatas { get; set; }
    }
}
