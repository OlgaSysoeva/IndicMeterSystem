﻿using System;

namespace IndicMeterSystem.Data.Entities
{
    /// <summary>
    /// Подтариф
    /// </summary>
    public class SubTariff
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Id тарифа
        /// </summary>
        public int TariffId { get; set; }

        /// <summary>
        /// Порядковый номер подтариыа в тарифе. Начинается с "1" для каждого тарифа
        /// </summary>
        public int Number { get; set; }

        /// <summary>
        /// Для тарифов по показаниям (тип 2 и 3) - цена за единицу, для фиксированного платежа (тип 1) - сумма платежа за месяц
        /// </summary>
        public double Price { get; set; }

        /// <summary>
        /// Дата добавления тарифа
        /// </summary>
        public DateTime AddDate { get; set; }

        /// <summary>
        /// Тариф
        /// </summary>
        public Tariff Tariff { get; set; }
    }
}
