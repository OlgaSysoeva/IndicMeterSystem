﻿using System;
using System.Collections.Generic;

namespace IndicMeterSystem.Data.Entities
{
    /// <summary>
    /// Тариф
    /// </summary>
    public class Tariff
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Id типа тарифа
        /// </summary>
        public int TariffTypeId { get; set; }

        /// <summary>
        /// Название тарифа
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Единицы измерения - для тарифов по показаниям (тип 2 и 3); для фиксированного платежа не используется
        /// </summary>
        public string Unit { get; set; }

        /// <summary>
        /// Признак удаления записи тарифа
        /// </summary>
        public bool Deleted { get; set; }

        /// <summary>
        /// Тип тарифа
        /// </summary>
        public TariffType TariffType { get; set; }

        /// <summary>
        /// Список подтарифов данного тарифа
        /// </summary>
        public IEnumerable<SubTariff> SubTariffs { get; set; }

    }
}
