﻿using System.Threading;

namespace IndicMeterSystem.Data
{
    /// <summary>
    /// Класс работы с семафором.
    /// </summary>
    public static class SemaphoreSetting
    {
        /// <summary>
        /// Переменная семафора.
        /// </summary>
        public static SemaphoreSlim Pool;

        // Количество допустимых потоков в семафоре.
        public static int MaximumThreadsToAccessDatabase;

        /// <summary>
        /// Инициализация семафора.
        /// </summary>
        public static void Init()
        {
            Pool = new SemaphoreSlim(
                MaximumThreadsToAccessDatabase,
                MaximumThreadsToAccessDatabase);
        }
    }
}
