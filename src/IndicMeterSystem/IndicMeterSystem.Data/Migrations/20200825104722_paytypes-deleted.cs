﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace IndicMeterSystem.Data.Migrations
{
    public partial class paytypesdeleted : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SubTariffs_PayTypes_PayTypeId",
                table: "SubTariffs");

            migrationBuilder.DropTable(
                name: "PayTypes");

            migrationBuilder.DropIndex(
                name: "IX_SubTariffs_PayTypeId",
                table: "SubTariffs");

            migrationBuilder.DropColumn(
                name: "PayTypeId",
                table: "SubTariffs");

            migrationBuilder.AddColumn<bool>(
                name: "Deleted",
                table: "Tariffs",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Deleted",
                table: "SubTariffs",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Deleted",
                table: "Tariffs");

            migrationBuilder.DropColumn(
                name: "Deleted",
                table: "SubTariffs");

            migrationBuilder.AddColumn<int>(
                name: "PayTypeId",
                table: "SubTariffs",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "PayTypes",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Title = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PayTypes", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_SubTariffs_PayTypeId",
                table: "SubTariffs",
                column: "PayTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_SubTariffs_PayTypes_PayTypeId",
                table: "SubTariffs",
                column: "PayTypeId",
                principalTable: "PayTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
