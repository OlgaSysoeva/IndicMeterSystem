﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан программой.
//     Исполняемая версия:4.0.30319.42000
//
//     Изменения в этом файле могут привести к неправильной работе и будут потеряны в случае
//     повторной генерации кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IndicMeterSystem.Common.Properties {
    using System;
    
    
    /// <summary>
    ///   Класс ресурса со строгой типизацией для поиска локализованных строк и т.д.
    /// </summary>
    // Этот класс создан автоматически классом StronglyTypedResourceBuilder
    // с помощью такого средства, как ResGen или Visual Studio.
    // Чтобы добавить или удалить член, измените файл .ResX и снова запустите ResGen
    // с параметром /str или перестройте свой проект VS.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "16.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class ErrorMessages {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal ErrorMessages() {
        }
        
        /// <summary>
        ///   Возвращает кэшированный экземпляр ResourceManager, использованный этим классом.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("IndicMeterSystem.Common.Properties.ErrorMessages", typeof(ErrorMessages).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Перезаписывает свойство CurrentUICulture текущего потока для всех
        ///   обращений к ресурсу с помощью этого класса ресурса со строгой типизацией.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Возникли ошибки при обращении к сервису..
        /// </summary>
        public static string IMS10001 {
            get {
                return ResourceManager.GetString("IMS10001", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Ошибка валидации модели..
        /// </summary>
        public static string IMS20001 {
            get {
                return ResourceManager.GetString("IMS20001", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Показания за текущий месяц уже внесены..
        /// </summary>
        public static string IMS30001 {
            get {
                return ResourceManager.GetString("IMS30001", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Новые показания меньше показаний за прошлый месяц..
        /// </summary>
        public static string IMS30002 {
            get {
                return ResourceManager.GetString("IMS30002", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Данные отсутствуют в базе..
        /// </summary>
        public static string IMS40001 {
            get {
                return ResourceManager.GetString("IMS40001", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Пользователь с таким Id не существует..
        /// </summary>
        public static string IMS50001 {
            get {
                return ResourceManager.GetString("IMS50001", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Пользователь уже переведен в архив..
        /// </summary>
        public static string IMS50002 {
            get {
                return ResourceManager.GetString("IMS50002", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Пользователь с такими учетными данными отсутствует в базе данных..
        /// </summary>
        public static string IMS50003 {
            get {
                return ResourceManager.GetString("IMS50003", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Пользователь уже зарегистрирован..
        /// </summary>
        public static string IMS50004 {
            get {
                return ResourceManager.GetString("IMS50004", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на У пользователя с таким Id нет зарегистрированных адресов..
        /// </summary>
        public static string IMS60001 {
            get {
                return ResourceManager.GetString("IMS60001", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Адрес с таким Id не существует..
        /// </summary>
        public static string IMS60002 {
            get {
                return ResourceManager.GetString("IMS60002", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Адрес уже зарегистрирован..
        /// </summary>
        public static string IMS60003 {
            get {
                return ResourceManager.GetString("IMS60003", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Адрес уже переведен в архив..
        /// </summary>
        public static string IMS60004 {
            get {
                return ResourceManager.GetString("IMS60004", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Счетчик не найден.
        /// </summary>
        public static string IMS70001 {
            get {
                return ResourceManager.GetString("IMS70001", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Счетчик уже переведен в архив...
        /// </summary>
        public static string IMS70002 {
            get {
                return ResourceManager.GetString("IMS70002", resourceCulture);
            }
        }
    }
}
