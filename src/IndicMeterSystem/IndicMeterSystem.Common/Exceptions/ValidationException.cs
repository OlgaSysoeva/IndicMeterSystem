﻿using System;
using System.Net;
using IndicMeterSystem.Common.Properties;

namespace IndicMeterSystem.Common.Exceptions
{
	/// <summary>
	/// Класс для исключений о невалидных данных в запросе.
	/// </summary>
	public class ValidationException : AppException
	{
		public ValidationErrorDetail Details { get; set; }

		/// <summary>
		/// Создает экземпляр класса <see cref="ValidationException"/>.
		/// </summary>
		/// <param name="errorMessage">Текст ошибки.</param>
		public ValidationException(string propertyName, string errorMessage)
			: base(nameof(ErrorMessages.IMS20001), HttpStatusCode.BadRequest)
		{
			Details = new ValidationErrorDetail { PropertyName = propertyName, ValidationMessage = errorMessage };
		}
	}

	/// <summary>
	/// Ошибка валидации.
	/// </summary>
	public class ValidationErrorDetail
	{
		/// <summary>
		/// Название поля с ошибкой.
		/// </summary>
		public string PropertyName { get; set; }

		/// <summary>
		/// Описание ошибки.
		/// </summary>
		public string ValidationMessage { get; set; }
	}
}
