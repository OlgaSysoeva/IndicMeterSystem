using AutoMapper;
using IndicMeterSystem.Data.Entities;
using IndicMeterSystem.Data.Interfaces;
using IndicMeterSystem.Services;
using IndicMeterSystem.Services.Models;
using IndicMeterSystem.Services.Services;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;
using Xunit;

namespace XUnitTest.Services
{
    public class UnitTestMeterServices
    {
        // свойство подмены репозитория обращения к БД
        private Mock<IRepository<Meter>> _meterRepository;

        // Проверка сервиса получения счетчиков по AdressId
        [Fact]
        public async void MeterServiceGetByAddressIdAsync()
        {
            // новая заглушка репозитория
            _meterRepository = new Mock<IRepository<Meter>>();
            // Ожидаемая модель
            var expectModel = new MeterModel() { Id = 1, AddressId = 1, SerialNumber = 12314, TariffId = 1, Description = "asdas" };
            IEnumerable<MeterModel> metersmodel = new List<MeterModel>() { expectModel };
            // фейковая коллекция счётчиков
            IEnumerable<Meter> meters = new List<Meter>() { new Meter() { Id = 1, AddressId = 1, SerialNumber = 12314, TariffId = 1, Description = "asdas" } };
            // заглушка БД возвращает фейковую коллекцию
            _meterRepository.Setup(x => x.GetOnConditionAsync(It.IsAny<Expression<Func<Meter,bool>>>())).ReturnsAsync(meters);

            // Конфигурация маппера
            var Config = new MapperConfiguration(cfg => cfg.CreateMap<Meter, MeterModel>().ReverseMap());
            //Создаём маппер
            var map = Config.CreateMapper();

            var meterservise = new MeterService(_meterRepository.Object, map);

            //получения коллекции счетчиков
            var metersmodels = await meterservise.GetByAddressIdAsync(1);

            // сверка полученного результата с ожидаемым
            Assert.Equal(metersmodels.Select(x => x.AddressId).First(), expectModel.AddressId);




        }
    }
}
