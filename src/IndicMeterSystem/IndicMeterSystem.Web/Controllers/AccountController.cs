﻿using AutoMapper;

using IndicMeterSystem.Services.Interfaces;
using IndicMeterSystem.Services.Models;

using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace IndicMeterSystem.Web.Controllers
{
    /// <summary>
    /// Предоставляет методы для работы с аутентификацией пользователя.
    /// </summary>
    [ApiController]
    [Route("api/[controller]")]
    public class AccountController : Controller  
    {
        private readonly IUserService _userService;
        private readonly IMapper _mapper;
        private readonly ILogger<AccountController> _logger;

        /// <summary>
        /// Конструктор контроллера аутентификации пользователя. 
        /// </summary>
        public AccountController(
            IUserService userService,
            IMapper mapper,
            ILogger<AccountController> logger)
        {
            _userService = userService;
            _mapper = mapper;
            _logger = logger;
        }


        /// <summary>
        /// Вход в профиль.
        /// </summary>
        [HttpPost("login")]
        public async Task<IActionResult> Login(LoginModel loginModel)
        {
            //  Проверяем входящие данные
            UserModel userModel = await _userService.GetUserByLoginAsync(
                loginModel.Login, loginModel.Password);

            // Аутентификация
            await Authenticate(loginModel.Login);

            // Скрываем конфиденциальную информацию.
            userModel.Login = default;
            userModel.Password = default;

            _logger.LogTrace(
                LoggerMessageTemplates.MethodEndTemplateWithModelParameter,
                nameof(Login), userModel);

            return Ok(); 
        }


        /// <summary>
        /// Регистрация нового пользователя.
        /// </summary>
        [HttpPost("register")]
        public async Task<IActionResult> Register(RegisterModel registerModel)
        {
            UserModel userModel = _mapper.Map<UserModel>(registerModel);

            registerModel.Password = default;
            registerModel.Login = default;
            _logger.LogTrace(
                LoggerMessageTemplates.MethodStartTemplateWithModelParameter,
                nameof(Register), registerModel);

            // Добавляем пользователя в БД
            await _userService.AddUserAsync(userModel);

            // Аутентификация
            await Authenticate(registerModel.Login);

            return Ok();  
        }


        /// <summary>
        /// Аутентификация.
        /// </summary>
        private async Task Authenticate(string login)
        {
            // Создаем один claim
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, login)
            };

            // Создаем объект ClaimsIdentity
            ClaimsIdentity id = new ClaimsIdentity(
                claims, 
                "ApplicationCookie", 
                ClaimsIdentity.DefaultNameClaimType, 
                ClaimsIdentity.DefaultRoleClaimType);
            
            // Установка аутентификационных куки
            await HttpContext.SignInAsync(
                CookieAuthenticationDefaults.AuthenticationScheme, 
                new ClaimsPrincipal(id));
        }


        /// <summary>
        /// Выход из профиля.
        /// </summary>
        [Authorize]
        [HttpPost("logout")]
        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return Ok();
        }
    }
}