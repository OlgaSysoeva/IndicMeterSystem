﻿using System.Threading.Tasks;

using IndicMeterSystem.Services.Interfaces;
using IndicMeterSystem.Services.Models;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace IndicMeterSystem.Web.Controllers
{
    /// <summary>
    /// Предоставляет методы для работы с платежами.
    /// </summary>
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class PaymentController : ControllerBase
    {
        private readonly IPaymentService _paymentService;
        private readonly ILogger<PaymentController> _logger;

        /// <summary>
        /// Конструктор контроллера платежей.
        /// </summary>
        /// <param name="paymentService">Объект интерфейса по работе с платежами.</param>
        public PaymentController(
            IPaymentService paymentService,
            ILogger<PaymentController> logger)
        {
            _paymentService = paymentService;
            _logger = logger;
        }

        /// <summary>
        /// Добавляет новую запись платежа.
        /// </summary>
        /// <param name="paymentModel">Модель данных платежа.</param>
        /// <returns>Возвращает статус код завершения.</returns>
        [HttpPost("add")]
        public async Task<IActionResult> Add(PaymentModel paymentModel)
        {
            _logger.LogTrace(
                LoggerMessageTemplates.MethodStartTemplateWithModelParameter,
                nameof(Add), paymentModel);

            await _paymentService.AddAsync(paymentModel);
            return Ok();
        }

        /// <summary>
        /// Получает список платежей по идентификатору показания.
        /// </summary>
        /// <param name="meterDataId">Идентификатор показания.</param>
        /// <returns>Возвращает список платежей.</returns>
        [HttpGet("meter/{meterDataId}")]
        public async Task<IActionResult> Get(int meterDataId)
        {
            _logger.LogTrace(
                LoggerMessageTemplates.MethodStartTemplateWithParameter,
                nameof(Get), meterDataId);

            var payment = await _paymentService.GetByMeterDataIdAsync(meterDataId);

            _logger.LogTrace(
                LoggerMessageTemplates.MethodEndTemplateWithModelParameter,
                nameof(Get), payment);

            return Ok(payment);
        }

        /// <summary>
        /// Получает оплаченную сумму по показанию счетчика.
        /// </summary>
        /// <param name="meterDataId">Идентификатор показания.</param>
        /// <returns>Возвращает оплаченную сумму.</returns>
        [HttpGet("paidsum/{meterDataId}")]
        public async Task<IActionResult> GetPaidlSum(int meterDataId)
        {
            _logger.LogTrace(
                LoggerMessageTemplates.MethodStartTemplateWithParameter,
                nameof(GetPaidlSum), meterDataId);

            var sum = await _paymentService.GetPaidSumAsync(meterDataId);

            _logger.LogTrace(
                LoggerMessageTemplates.MethodEndTemplateWithModelParameter,
                nameof(GetPaidlSum), sum);

            return Ok(sum);
        }

        /// <summary>
        /// Получает общую сумму оплаты показания счетчика.
        /// </summary>
        /// <param name="meterDataId">Идентификатор показания.</param>
        /// <returns>Возвращает общую сумму.</returns>
        [HttpGet("totalsum/{meterDataId}")]
        public async Task<IActionResult> GetTotalSum(int meterDataId)
        {
            _logger.LogTrace(
                LoggerMessageTemplates.MethodStartTemplateWithParameter,
                nameof(GetTotalSum), meterDataId);

            var sum = await _paymentService.GetTotalSumAsync(meterDataId);

            _logger.LogTrace(
                LoggerMessageTemplates.MethodEndTemplateWithModelParameter,
                nameof(GetTotalSum), sum);

            return Ok(sum);
        }
    }
}
