﻿using System;
using System.Threading.Tasks;

using IndicMeterSystem.Services.Interfaces;
using IndicMeterSystem.Services.Models;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace IndicMeterSystem.Web.Controllers
{
    /// <summary>
    /// Предоставляет методы для работы с показаниями.
    /// </summary>
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class MeterDataController : ControllerBase
    {
        private readonly IMeterDataService _meterDataService;
        private readonly ILogger<MeterDataController> _logger;

        /// <summary>
        /// Конструктор контроллера показаний.
        /// </summary>
        /// <param name="meterDataService">Объект интерфейса по работе с показаниями.</param>
        public MeterDataController(
            IMeterDataService meterDataService,
            ILogger<MeterDataController> logger)
        {
            _meterDataService = meterDataService;
            _logger = logger;
        }

        /// <summary>
        /// Добавляет новую запись показания.
        /// </summary>
        /// <param name="meterDataModel">Модель данных показания.</param>
        /// <returns>Возвращает статус код завершения.</returns>
        [HttpPost("add")]
        public async Task<IActionResult> AddMeterData(MeterDataModel meterDataModel)
        {
            _logger.LogTrace(
                LoggerMessageTemplates.MethodStartTemplateWithModelParameter,
                nameof(AddMeterData), meterDataModel);

            await _meterDataService.AddAsync(meterDataModel);

            return Ok();
        }

        /// <summary>
        /// Получает данные по показанию с данными по платежам.
        /// </summary>
        /// <param name="meterDataId">Идентификатор показания.</param>
        /// <returns>Возсращает модель показания со списком платежей.</returns>
        [HttpGet("{meterDataId}")]
        public async Task<IActionResult> GetById(int meterDataId)
        {
            _logger.LogTrace(
                LoggerMessageTemplates.MethodStartTemplateWithParameter,
                nameof(GetById), meterDataId);

            var meterDataModel = await _meterDataService.GetByIdWithPaymentsAsync(meterDataId);

            _logger.LogTrace(
                LoggerMessageTemplates.MethodEndTemplateWithModelParameter,
                nameof(GetById), meterDataModel);

            return Ok(meterDataModel);
        }

        /// <summary>
        /// Получает данные по показаниям за интервал времени.
        /// </summary>
        /// <param name="dateFrom">Начальная дата интервала.</param>
        /// <param name="dateTo">Конечная дата интервала.</param>
        /// <param name="meterId">Идентификатор счетчика, может быть не задан.</param>
        /// <returns>Возвращает список моделей показаний.</returns>
        [HttpGet("{dateFrom}&{dateTo}&{meterId}")]
        public async Task<IActionResult> GetByInterval(DateTime dateFrom, DateTime dateTo, int meterId = 0)
        {
            _logger.LogTrace(
                "Запущен метод {MethodName}. Входные данные: {Param1}, {Param2}, {Param3}",
                nameof(GetByInterval), dateFrom, dateFrom, meterId);

            var meterDataModels = await _meterDataService
                .GetByIntervalAsync(dateFrom, dateTo, meterId);

            _logger.LogTrace(
                LoggerMessageTemplates.MethodEndTemplateWithModelParameter,
                nameof(GetByInterval), meterDataModels);

            return Ok(meterDataModels);
        }
    }
}
