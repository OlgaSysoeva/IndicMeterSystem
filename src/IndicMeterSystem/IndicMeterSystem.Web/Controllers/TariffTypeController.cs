﻿using System.Collections.Generic;
using System.Threading.Tasks;

using IndicMeterSystem.Services.Interfaces;
using IndicMeterSystem.Services.Models;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;

namespace IndicMeterSystem.Web.Controllers
{
    /// <summary>
    /// Методы для работы с тпами тарифа
    /// </summary>
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class TariffTypeController : ControllerBase
    {
        private readonly ITariffTypeService _tariffTypeService;
        private readonly ILogger<TariffTypeController> _logger;

        public TariffTypeController(
            ITariffTypeService service,
            ILogger<TariffTypeController> logger)
        {
            _tariffTypeService = service;
            _logger = logger;
        }

        /// <summary>
        /// Получает список всех типов тарифов
        /// </summary>
        /// <returns>Список типов тарифов</returns>
        [HttpGet()]
        public async Task<IEnumerable<TariffTypeModel>> GetAll()
        {
            return await _tariffTypeService.GetAllAsync();
        }

        /// <summary>
        /// Получает тип тарифа по Id
        /// </summary>
        /// <returns>Тип тарифа</returns>
        [HttpGet("{tariffTypeId}")]
        public async Task<TariffTypeModel> GetTariffType(int tariffTypeId)
        {
            _logger.LogTrace(
                LoggerMessageTemplates.MethodStartTemplateWithParameter,
                nameof(GetTariffType), tariffTypeId);

            var tariffTypeModel = await _tariffTypeService.GetByIdAsync(tariffTypeId);

            _logger.LogTrace(
                LoggerMessageTemplates.MethodEndTemplateWithModelParameter,
                nameof(GetTariffType), tariffTypeModel);

            return tariffTypeModel;
        }

    }
}
