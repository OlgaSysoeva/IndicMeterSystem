﻿using System.Threading.Tasks;

using IndicMeterSystem.Services.Interfaces;
using IndicMeterSystem.Services.Models;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace IndicMeterSystem.Web.Controllers
{
    /// <summary>
    /// Методы для работы с подтарифами
    /// </summary>
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class SubTariffController : ControllerBase
    {
        private readonly ISubTariffService _subTariffService;
        private readonly ILogger<SubTariffController> _logger;

        /// <summary>
        /// Конструктор контроллера подтарифов
        /// </summary>
        /// <param name="subTariffService"></param>
        public SubTariffController(
            ISubTariffService subTariffService,
            ILogger<SubTariffController> logger)
        {
            _subTariffService = subTariffService;
            _logger = logger;
        }

        /// <summary>
        /// Получает список подтарифов по Id тарифа
        /// </summary>
        /// <param name="tariffId">Id тарифа</param>
        /// <returns>Список подтарифов</returns>
        [HttpGet("tariff/{tariffId}")]
        public async Task<IActionResult> GetSubTariff(int tariffId)
        {
            _logger.LogTrace(
                LoggerMessageTemplates.MethodStartTemplateWithParameter,
                nameof(GetSubTariff), tariffId);

            var subTariffs = await _subTariffService.GetByTariffIdAsync(tariffId);

            _logger.LogTrace(
                LoggerMessageTemplates.MethodEndTemplateWithModelParameter,
                nameof(GetSubTariff), subTariffs);

            return Ok(subTariffs);
        }

        /// <summary>
        /// Добавляет новый подтариф
        /// </summary>
        /// <param name="subTariff">Модель подтарифа</param>
        /// <returns>Статус кода завершения</returns>
        [HttpPost("add")]
        public async Task<IActionResult> AddSubTariff(SubTariffModel subTariff)
        {
            _logger.LogTrace(
                LoggerMessageTemplates.MethodStartTemplateWithModelParameter,
                nameof(AddSubTariff), subTariff);

            await _subTariffService.AddAsync(subTariff);
            return Ok();
        }

    }
}
