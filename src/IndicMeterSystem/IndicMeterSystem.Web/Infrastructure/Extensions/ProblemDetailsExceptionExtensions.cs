﻿using IndicMeterSystem.Common.Exceptions;
using Microsoft.AspNetCore.Mvc;

namespace IndicMeterSystem.Web.Infrastructure.Extensions
{
	/// <summary>
	/// Расшиерния для добавления информации об исключениях, производных от <see cref="AppException"/>, в описание ошибки <see cref="ProblemDetails"/>.
	/// </summary>
	public static class ProblemDetailsExceptionExtensions
	{
		/// <summary>
		/// Добавляет данные из исключения в описание проблемы.
		/// </summary>
		/// <param name="problemDetails">Описание проблемы.</param>
		/// <param name="exception">Исключение.</param>
		/// <returns>Описание с данными из исключения.</returns>
		public static ProblemDetails WithException(
			this ProblemDetails problemDetails,
			AppException exception)
		{
			return problemDetails
				.WithStatusCode(exception.StatusCode)
				.WithErrorCode(exception.ErrorCode)
				.WithErrorMessage(exception.Message);
		}

		/// <summary>
		/// Добавляет данные из исключения в описание проблемы.
		/// </summary>
		/// <param name="problemDetails">Описание проблемы.</param>
		/// <param name="exception">Исключение.</param>
		/// <returns>Описание с данными из исключения.</returns>
		public static ProblemDetails WithException(
			this ProblemDetails problemDetails,
			ValidationException exception)
		{
			return problemDetails
				.WithStatusCode(exception.StatusCode)
				.WithErrorCode(exception.ErrorCode)
				.WithErrorMessage(exception.Message)
				.WithErrorDetails(new {exception.Details.PropertyName, exception.Details.ValidationMessage});
		}
	}
}
