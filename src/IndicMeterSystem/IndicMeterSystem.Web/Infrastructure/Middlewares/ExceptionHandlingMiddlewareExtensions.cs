﻿using Microsoft.AspNetCore.Builder;

namespace IndicMeterSystem.Web.Infrastructure.Middlewares
{
	/// <summary>
	/// Расширения для <see cref="ExceptionHandlingMiddleware"/>.
	/// </summary>
	public static class ExceptionHandlingMiddlewareExtensions
	{
		/// <summary>
		/// Встраивает <see cref="ExceptionHandlingMiddleware"/> в конвейер обработки запросов.
		/// </summary>
		/// <param name="builder">Application builder.</param>
		/// <returns>Application builder with exception handling.</returns>
		public static IApplicationBuilder UseExceptionHandling(this IApplicationBuilder builder)
		{
			return builder.UseMiddleware<ExceptionHandlingMiddleware>();
		}
	}
}
