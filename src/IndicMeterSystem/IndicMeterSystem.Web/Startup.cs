using System;

using AutoMapper;

using IndicMeterSystem.Data;
using IndicMeterSystem.Data.EF;
using IndicMeterSystem.Data.Interfaces;
using IndicMeterSystem.Data.Repositories;
using IndicMeterSystem.Services.Services;
using IndicMeterSystem.Services.Interfaces;
using IndicMeterSystem.Services;
using IndicMeterSystem.Web.Infrastructure.Middlewares;


using FluentValidation.AspNetCore;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authentication.Cookies;

namespace IndicMeterSystem.Web
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            // получаем строку подключения из файла конфигурации
            string connection = Configuration.GetConnectionString("DefaultConnection");

            services.AddSwaggerGen(c =>
            {
                c.IncludeXmlComments(GetXmlCommentsPath());
            });

            SemaphoreSetting.MaximumThreadsToAccessDatabase = Configuration.GetValue(
                "MaximumThreadsToAccessDatabase", 3);

            // добавляем контекст в качестве сервиса в приложение
            services.AddDbContext<ApplicationContext>(options => options.UseNpgsql(connection));

            // Добавляем валидаторы
            services
                .AddMvc()
                .AddFluentValidation(fv => fv.RegisterValidatorsFromAssemblyContaining<Startup>());

            // добавляем связи интерфейсов с реализацией
            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            services.AddScoped<IPaymentService, PaymentService>();
            services.AddScoped<IMeterDataService, MeterDataService>();
            services.AddScoped<IMeterService, MeterService>();
            services.AddScoped<ISubTariffService, SubTariffService>();
            services.AddScoped<IAddressService, AddressService>();
            services.AddScoped<ITariffTypeService, TariffTypeService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<ITariffService, TariffService>();

            // настраиваем automapper
            services.AddAutoMapper(typeof(AutoMapperProfile));

            // установка конфигурации службы для проверки подлинности
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                .AddCookie(options =>
                {
                    options.LoginPath = new Microsoft.AspNetCore.Http.PathString("/api/Account/login/");
                    options.AccessDeniedPath = new Microsoft.AspNetCore.Http.PathString("/api/Account/register/");
                    options.LogoutPath = new Microsoft.AspNetCore.Http.PathString("/api/Account/logout/");
                });

            // используем контроллеры без представлений
            services.AddControllers();
        }

        private static string GetXmlCommentsPath() => $@"{AppDomain.CurrentDomain.BaseDirectory}\Swagger.XML";

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            // Enable middleware to serve generated Swagger as a JSON endpoint
            app.UseSwagger();

            // Enable middleware to serve swagger-ui assets (HTML, JS, CSS etc.)
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Test API V1");
            });

            // Логирование запросов.
            app.UseRequestResponseLogging();

            // Обработка исключений.
            app.UseExceptionHandling();

            app.UseRouting();

            // Аутентификация.
            app.UseAuthentication();

            // Авторизация.
            app.UseAuthorization();

            // Подключаем маршрутизацию на контроллеры.
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
