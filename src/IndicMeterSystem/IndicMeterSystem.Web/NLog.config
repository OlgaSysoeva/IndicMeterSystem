<?xml version="1.0" encoding="utf-8" ?>
<nlog xmlns="http://www.nlog-project.org/schemas/NLog.xsd"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      autoReload="true"
      internalLogLevel="Off"
      internalLogFile="c:\temp\internal-nlog.txt">

  <!-- enable asp.net core layout renderers -->
  <extensions>
    <add assembly="NLog.Web.AspNetCore"/>
  </extensions>

  <!-- the targets to write to -->
  <targets>
    <!-- write logs to file  -->
    <target xsi:type="File" name="allfile" fileName="${basedir}\Logs\nlog-all-${shortdate}.log"
            layout="${longdate}| ${event-properties:item=EventId_Id} |${pad:padding=-5:fixedlength=true:inner=${level:uppercase=true}}| ${logger} | ${message} ${exception:format=tostring}" 
			openFileCacheTimeout ="30"
            archiveFileName="${basedir}/Logs/nlog-all-${date:format=yyyyMMdd_HHmmss}.{####}.txt"
            archiveNumbering="Sequence"
			archiveAboveSize="10485760"
			concurrentWrites="true"
            archiveOldFileOnStartup ="true"
            maxArchiveFiles="30"
            createDirs="true"
            keepFileOpen ="true"
			/>

    <!-- another file log, only own logs. Uses some ASP.NET core renderers -->
    <target xsi:type="File" name="ownFile-web" fileName="${basedir}\Logs\nlog-own-${shortdate}.log"
            layout="${longdate}| ${event-properties:item=EventId_Id} |${pad:padding=-5:fixedlength=true:inner=${level:uppercase=true}}| ${logger} | ${message} ${exception:format=tostring} |url: ${aspnet-request-url}|action: ${aspnet-mvc-action}"
			openFileCacheTimeout ="30"
            archiveFileName="${basedir}/Logs/nlog-own-${date:format=yyyyMMdd_HHmmss}.{####}.txt"
            archiveNumbering="Sequence"
			archiveAboveSize="10485760"
			concurrentWrites="true"
            archiveOldFileOnStartup ="true"
            maxArchiveFiles="30"
            createDirs="true"
            keepFileOpen ="true"
			/>
  </targets>

  <!-- rules to map from logger name to target -->
  <rules>
    <!--All logs, including from Microsoft-->
    <logger name="*" minlevel="Trace" writeTo="allfile" />

    <!--Skip non-critical Microsoft logs and so log only own logs-->
    <logger name="Microsoft.*" maxlevel="Info" final="true" /> <!-- BlackHole without writeTo -->
    <logger name="*" minlevel="Trace" writeTo="ownFile-web" />
  </rules>
</nlog>
