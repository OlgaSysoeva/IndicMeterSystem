﻿using FluentValidation;
using IndicMeterSystem.Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IndicMeterSystem.Web.Validators
{
    /// <summary>
    /// Валидатор для <see cref="PaymentModel"/>.<br/>
    /// </summary>

    public class PaymentValidator : AbstractValidator<PaymentModel>
    {
        /// <inheridoc/>
        public PaymentValidator()
        {
            RuleFor(x => x.Amount).Cascade(CascadeMode.Stop).NotEmpty().NotEqual(0).WithMessage("Сумма платежа не должна быть равна нулю.");
        }
    }
}
