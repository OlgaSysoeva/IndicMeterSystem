﻿using FluentValidation;
using IndicMeterSystem.Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IndicMeterSystem.Web.Validators
{
    /// <summary>
    /// Валидатор для <see cref="RegisterModel"/>.<br/>
    /// </summary>
    public class RegisterValidator : AbstractValidator<RegisterModel>
    {
        public RegisterValidator()
        {
            RuleFor(x => x.UserName).NotEmpty().WithMessage("Не указано имя пользователя");
            RuleFor(x => x.Login).NotEmpty().WithMessage("Не указан логин");
            RuleFor(x => x.Password).NotEmpty().WithMessage("Не указан пароль");
            RuleFor(x => x.Password).Equal(x => x.ConfirmPassword).WithMessage("Пароль введен неверно");
            //Каскадная проверка. Остановка на первом несовпадении.
            RuleFor(x => x.Email).Cascade(CascadeMode.Stop).NotEmpty().WithMessage("{PropertyName} is required.")
                .EmailAddress()
                .WithMessage("A valid {PropertyName} is required.");

        }
    }
}
