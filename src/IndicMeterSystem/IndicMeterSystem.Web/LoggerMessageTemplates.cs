﻿namespace IndicMeterSystem.Web
{
    /// <summary>
    /// Хранит шаблоны сообщений для логирования.
    /// </summary>
    public static class LoggerMessageTemplates
    {
        /// <summary>
        /// Шаблон сообщения для логирования старта работы метода с входным параметром.
        /// </summary>
        public static string MethodStartTemplateWithParameter = 
            "Запущен метод {MethodName}. Входные данные: {Param}";

        /// <summary>
        /// Шаблон сообщения для логирования старта работы метода с входным параметром-моделью.
        /// </summary>
        public static string MethodStartTemplateWithModelParameter = 
            "Запущен метод {MethodName}. Входные данные: {@Param}";

        /// <summary>
        /// Шаблон сообщения для логирования окончания работы метода с входным параметром-моделью.
        /// </summary>
        public static string MethodEndTemplateWithModelParameter = 
            "Результат работы метода {MethodName}: {@Param}";
    }
}
