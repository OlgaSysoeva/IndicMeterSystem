﻿using System.Threading.Tasks;
using IndicMeterSystem.Services.Models;

namespace IndicMeterSystem.Services.Interfaces
{
    /// <summary>
    /// Сервис для работы с тарифами
    /// </summary>
    public interface ITariffService
    {
        /// <summary>
        /// Получение модели тарифа по Id
        /// </summary>
        Task<TariffExtendedModel> GetByIdAsync(int tariffId);
        /// <summary>
        /// Добавление тарифа
        /// </summary>
        Task AddAsync(TariffExtendedModel tariffModel);
        /// <summary>
        /// Удаление тарифа с указанным Id
        /// </summary>
        Task DeleteAsync(int tariffId);
        /// <summary>
        /// Обновление в БД записи тарифа вместе с подтарифами
        /// </summary>
        /// <param name="tariffModel">измененный тариф</param>
        /// <returns></returns>
        Task UpdateAsync(TariffExtendedModel tariffModel);
    }
}
