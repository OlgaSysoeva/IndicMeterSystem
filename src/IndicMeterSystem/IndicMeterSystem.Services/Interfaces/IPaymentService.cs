﻿using System.Collections.Generic;
using System.Threading.Tasks;

using IndicMeterSystem.Services.Models;

namespace IndicMeterSystem.Services.Interfaces
{
    /// <summary>
    /// Сервис для работы с платежами.
    /// </summary>
    public interface IPaymentService
    {
        /// <summary>
        /// Добавляет новый платеж в базу.
        /// </summary>
        /// <param name="paymentModel">Модель с данными  платеже.</param>
        /// <returns></returns>
        Task<PaymentModel> AddAsync(PaymentModel paymentModel);

        /// <summary>
        /// Метод получает платежи по id показания.
        /// </summary>
        /// <param name="meterDataId">Идентификатор показания.</param>
        /// <returns>Возвращает список платежей.</returns>
        Task<IEnumerable<PaymentModel>> GetByMeterDataIdAsync(int meterDataId);

        /// <summary>
        /// Получает оплаченную сумму по показанию.
        /// </summary>
        /// <param name="meterDataId">Идентификатор показания.</param>
        /// <returns>Возвращает сумму платежей.</returns>
        Task<double> GetPaidSumAsync(int meterDataId);

        /// <summary>
        /// Получает общую сумму оплаты показания счетчика.
        /// </summary>
        /// <param name="meterDataId">Идентификатор показания.</param>
        /// <returns>Возвращает общую сумму.</returns>
        Task<double> GetTotalSumAsync(int meterDataId);

        /// <summary>
        /// Изменяет значение статуса оплаты.
        /// </summary>
        /// <param name="meterDataId">Идентификатор показания.</param>
        /// <param name="paymentState">Новый статус оплаты.</param>
        /// <returns></returns>
        Task UpdatePaymentStatusAsync(int meterDataId, int paymentState);
    }
}
