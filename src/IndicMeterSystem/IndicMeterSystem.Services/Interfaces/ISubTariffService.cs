﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using IndicMeterSystem.Services.Models;

namespace IndicMeterSystem.Services.Interfaces
{
    /// <summary>
    /// Сервис для работы с подтарифами
    /// </summary>
    public interface ISubTariffService
    {

        /// <summary>
        /// Получаем по Id тарифа список подтарифов с максимальной датой
        /// </summary>
        /// <param name="tariffId">Id тарифа для выбора подтарифа</param>
        /// <param name="maxDate">Выбираем только записи с датой, меньшей данного значения. Если null, то ищем по всем записям</param>
        /// <returns>Список подтарифов указанного тарифа с максимальной датой</returns>
        Task<IEnumerable<SubTariffModel>> GetByTariffIdAsync(int tariffId, DateTime? maxDate = null);

        /// <summary>
        /// Создание нового подтарифа
        /// </summary>
        Task AddAsync(SubTariffModel subTariff);
    }
}
