﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using IndicMeterSystem.Services.Models;

namespace IndicMeterSystem.Services.Interfaces
{
    /// <summary>
    /// Сервис для работы с типами тарифа
    /// </summary>
    public interface ITariffTypeService
    {
        /// <summary>
        /// Получаем список всех типов тарифов
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<TariffTypeModel>> GetAllAsync();

        /// <summary>
        /// Получаем тип тарифа по Id
        /// </summary>
        /// <param name="tariffId">Id типа тарифа</param>
        /// <returns>Тип тарифа</returns>
        Task<TariffTypeModel> GetByIdAsync(int tariffTypeId);
    }
}
