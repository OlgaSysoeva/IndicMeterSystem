﻿using AutoMapper;
using IndicMeterSystem.Data.Entities;
using IndicMeterSystem.Services.Models;
using System;

namespace IndicMeterSystem.Services
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<MeterData, MeterDataModel>().ReverseMap();

            CreateMap<Payment, PaymentModel>().ReverseMap();

            CreateMap<TariffType, TariffTypeModel>().ReverseMap();

            CreateMap<Tariff, TariffExtendedModel>().ReverseMap();

            CreateMap<SubTariff, SubTariffModel>();

            CreateMap<SubTariffModel, SubTariff>()
                .ForMember(dest => dest.AddDate, opt => opt.MapFrom(src => DateTime.Now))
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => 0));

            CreateMap<MeterData, MeterDataWithPaymentsModel>();
            
            CreateMap<MeterData, MeterDataWithSumModel>();

            CreateMap<MeterValue, MeterValueModel>().ReverseMap();

            CreateMap<Meter, MeterModel>().ReverseMap();

            CreateMap<Address, InclusiveAddressModel>();

            CreateMap<User, UserModel>();

            CreateMap<UserModel, User>()
                .ForMember(dest => dest.UserName, opt => opt.MapFrom(c => c.UserName.Trim()))
                .ForMember(dest => dest.Login, opt => opt.MapFrom(src => src.Login.Trim().ToLower()))
                .ForMember(dest => dest.Password, opt => opt.MapFrom(src => src.Password.Trim()))
                .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.Email.Trim()))
                .ForMember(dest => dest.PhoneNumber, opt => opt.MapFrom(src => src.PhoneNumber.Trim()));

            CreateMap<Address, AddressModel>().ReverseMap();

            CreateMap<UserModel, RegisterModel>().ReverseMap();
        }
    }
}
