﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace IndicMeterSystem.Services.Models
{
    /// <summary>
    /// Модель с данными показания.
    /// </summary>
    public class MeterDataModel
    {
        /// <summary>
        /// Идентификатор.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Дата добавления.
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Комментарий.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Статус оплаты показания.
        /// </summary>
        public int PaymentState { get; set; }

        /// <summary>
        /// Список значений показаний.
        /// </summary>
        public IEnumerable<MeterValueModel> MeterValues { get; set; }

        /// <summary>
        /// Идентификатор счетчика.
        /// </summary>
        public int MeterId { get; set; }
    }
}
