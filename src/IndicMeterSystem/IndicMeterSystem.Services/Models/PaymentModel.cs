﻿using System;
using System.Text.Json.Serialization;

namespace IndicMeterSystem.Services.Models
{
    /// <summary>
    /// Модель данных по оплате показаний.
    /// </summary>
    public class PaymentModel
    {
        /// <summary>
        /// Идентификатор.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Дата оплаты.
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Сумма оплаты.
        /// </summary>
        public double Amount { get; set; }

        /// <summary>
        /// Идентификатор показания.
        /// </summary>
        public int MeterDataId { get; set; }
    }
}
