﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace IndicMeterSystem.Services.Models
{
    /// <summary>
    /// Расширенная модель тарифа (со списком подтарифов)
    /// </summary>
    public class TariffExtendedModel
    {
        /// <summary>
        /// Идентификатор тарифа
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Id типа тарифа
        /// </summary>
        public int TariffTypeId { get; set; }

        /// <summary>
        /// Название тарифа
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Единицы измерения - для тарифов по показаниям (тип 2 и 3); для фиксированного платежа не используется
        /// </summary>
        public string Unit { get; set; }

        /// <summary>
        /// Список подтарифов данного тарифа
        /// </summary>
        public List<SubTariffModel> SubTariffs { get; set; }

        /// <summary>
        /// Тип тарифа
        /// </summary>
        public TariffTypeModel TariffType { get; set; }
    }
}
