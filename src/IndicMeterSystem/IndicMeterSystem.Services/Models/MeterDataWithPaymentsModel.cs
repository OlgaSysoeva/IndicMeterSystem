﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace IndicMeterSystem.Services.Models
{
    /// <summary>
    /// Модель с данными показания.
    /// </summary>
    public class MeterDataWithPaymentsModel
    {
        /// <summary>
        /// Идентификатор.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Дата добавления.
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Комментарий.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Идентификатор счетчика.
        /// </summary>
        public int MeterId { get; set; }

        /// <summary>
        /// Список связанных платежей.
        /// </summary>
        public IEnumerable<PaymentModel> Payments { get; set; }

        /// <summary>
        /// Список значений показаний.
        /// </summary>
        public IEnumerable<MeterValueModel> MeterValues { get; set; }

        /// <summary>
        /// Итоговая сумма оплаты.
        /// </summary>
        public double TotalSum { get; set; }

        /// <summary>
        /// Остаток для оплаты.
        /// </summary>
        public double Residue { get; set; }
    }
}

