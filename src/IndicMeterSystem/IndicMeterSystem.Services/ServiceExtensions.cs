﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using IndicMeterSystem.Common.Exceptions;

namespace IndicMeterSystem.Services
{
	public static class ServiceExtensions
	{
		/// <summary>
		/// Проверяет, что элемент существует.
		/// </summary>
		/// <typeparam name="TItem">Тип элемента.</typeparam>
		/// <param name="item">Элемент (операция)</param>
		/// <param name="errorCode">Код ошибки.</param>
		/// <returns>Элемент.</returns>
		public static async Task<TItem> EnsureExists<TItem>(
			this Task<TItem> item,
			string errorCode)
			where TItem : class
		{
			var value = await item;

			if (value == null)
				throw new NotFoundException(errorCode);

			return value;
		}

		/// <summary>
		/// Проверяет, что элемент существует.
		/// </summary>
		/// <typeparam name="TItem">Тип элемента.</typeparam>
		/// <param name="item">Элемент (операция)</param>
		/// <param name="errorCode">Код ошибки.</param>
		/// <returns>Элемент.</returns>
		public static TItem EnsureExists<TItem>(
			this TItem item,
			string errorCode)
			where TItem : class
		{
			if (item == null)
				throw new NotFoundException(errorCode);

			return item;
		}

		/// <summary>
		/// Проверяет, что коллекция не пуста.
		/// </summary>
		/// <typeparam name="TItem">Тип коллекции.</typeparam>
		/// <param name="items">Коллекция.</param>
		/// <param name="errorCode">Код ошибки.</param>
		/// <returns>Коллекция.</returns>
		public static IEnumerable<TItem> EnsureNotEmpty<TItem>(
			this IEnumerable<TItem> items,
			string errorCode)
			where TItem : class
		{
			if (items == null || items.Count() == 0)
			{
				throw new NotFoundException(errorCode);
			}

			return items;
		}
	}
}
