﻿using AutoMapper;

using IndicMeterSystem.Common.Exceptions;
using IndicMeterSystem.Common.Properties;
using IndicMeterSystem.Data.Entities;
using IndicMeterSystem.Data.Interfaces;
using IndicMeterSystem.Services.Interfaces;
using IndicMeterSystem.Services.Models;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IndicMeterSystem.Services.Services
{
    public class AddressService : IAddressService
    {
        private readonly IRepository<Address> _addressRepository;
        private readonly IMapper _mapper;

        public AddressService(
            IRepository<Address> addressRepository,
            IMapper mapper)
        {
            _addressRepository = addressRepository;
            _mapper = mapper;
        }
         
        public async Task<IEnumerable<AddressModel>> GetByUserIdAsync(int userId)
        {
            var address = await _addressRepository.GetOnConditionAsync(m => m.UserId == userId);

            address.EnsureNotEmpty(nameof(ErrorMessages.IMS60001));

            var addressModel = address                
                .Select(m => new AddressModel()
                {
                    Id = m.Id,
                    RegistrationAddress = m.RegistrationAddress,
                    Name = m.Name,
                    Deleted = m.Deleted,
                });

            return addressModel;
        }

        public async Task AddAddressAsync(AddressModel addressModel)
        {
            Address address = _mapper.Map<Address>(addressModel);

            var addressExist = await _addressRepository.AnyAsync(x =>
                x.RegistrationAddress == address.RegistrationAddress);

            if (addressExist)
            {
                throw new ServiceException(nameof(ErrorMessages.IMS60003));
            }

            await _addressRepository.AddAsync(address);
        }

        public async Task DeleteAddressAsync(int addressId)
        {
            var address = await _addressRepository.GetAsync(addressId);

            address.EnsureExists(nameof(ErrorMessages.IMS60002));

            if (address.Deleted)
            {
                throw new ServiceException(nameof(ErrorMessages.IMS60004));
            }

            address.Deleted = true;
            await _addressRepository.UpdateAsync(address);
        }

        public async Task<InclusiveAddressModel> AddCopyAsync(int addressId)
        {
            // Находим в базе по id.
            var address = (await _addressRepository.GetWithIncludeOnConditionAsync(
                nameof(Address.Meters),
                x => x.Id ==  addressId))
                .FirstOrDefault();

            address.EnsureExists(nameof(ErrorMessages.IMS40001));

            // Копируем.
            var addressCopy = address.DeepCopy();

            // Сохраняем в базу.
            var addressDataCopy = await _addressRepository.AddAsync(addressCopy);

            var addressModelCopy = _mapper.Map<InclusiveAddressModel>(addressDataCopy);
            return addressModelCopy;
        }

        public async Task UpdateAddressAsync(AddressModel addressModel)
        {
            // Проверяем наличие адреса в БД
            var address = await _addressRepository.GetAsync(addressModel.Id);

            address.EnsureExists(nameof(ErrorMessages.IMS60002));

            address.UserId = addressModel.UserId;
            address.RegistrationAddress = addressModel.RegistrationAddress;
            address.Name = addressModel.Name;

            await _addressRepository.UpdateAsync(address);
        }
    }
}
