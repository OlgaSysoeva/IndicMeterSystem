using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;

using AutoMapper;

using IndicMeterSystem.Common.Exceptions;
using IndicMeterSystem.Common.Properties;
using IndicMeterSystem.Data.Entities;
using IndicMeterSystem.Data.Enums;
using IndicMeterSystem.Data.Interfaces;
using IndicMeterSystem.Services.Interfaces;
using IndicMeterSystem.Services.Models;

namespace IndicMeterSystem.Services.Services
{
    /// <summary>
    /// Реализует интерфейс платежей
    /// </summary>
    public class PaymentService : IPaymentService
    {
        private readonly IRepository<Payment> _paymentRepository;
        private readonly ISubTariffService _subTariffService;
        private readonly IRepository<MeterData> _meterDataRepository;
        private readonly IMeterService _meterService;
        private readonly IMapper _mapper;

        public PaymentService(
            IRepository<Payment> paymentRepository,
            ISubTariffService subTariffService,
            IRepository<MeterData> meterDataRepository,
            IMeterService meterService,
            IMapper mapper)
        {
            _paymentRepository = paymentRepository;
            _subTariffService = subTariffService;
            _meterDataRepository = meterDataRepository;
            _meterService = meterService;
            _mapper = mapper;
        }

        public async Task<PaymentModel> AddAsync(PaymentModel paymentModel)
        {
            if (paymentModel == null)
            {
                throw new ValidationException(nameof(paymentModel),
                    "Аргумент не может быть пустым.");
            }

            if (paymentModel.Amount == 0)
            {
                throw new ValidationException(nameof(paymentModel),
                    "Сумма платежа не должна быть равна нулю.");
            }

            var meterDataId = paymentModel.MeterDataId;
            var totalSum = await GetTotalSumAsync(meterDataId);
            var paidSum = await GetPaidSumAsync(meterDataId);

            if (paymentModel.Amount > (totalSum - paidSum))
            {
                throw new ServiceException(nameof(ErrorMessages.IMS10001),
                    "Сумма оплаты превышает остаток.");
            }

            using var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);

            var payment = _mapper.Map<PaymentModel, Payment>(
                paymentModel,
                opts => opts.AfterMap((s, d) =>
                {
                    d.Date = DateTime.Now;
                }));

            var addPaymentModel = _mapper.Map<PaymentModel>(
                await _paymentRepository.AddAsync(payment));

            var paymentState = PaymentStateEnum.None;
            var paymentBalance = totalSum - (paidSum + addPaymentModel.Amount);

            if (paymentBalance == 0)
            {
                paymentState = PaymentStateEnum.Full;
            }
            else if (paymentBalance > 0)
            {
                paymentState = PaymentStateEnum.Partial;
            }

            await UpdatePaymentStatusAsync(meterDataId, (int)paymentState);

            scope.Complete();

            return addPaymentModel;
        }

        public async Task<IEnumerable<PaymentModel>> GetByMeterDataIdAsync(int meterDataId)
        {
            var paymentData = await _paymentRepository
                .GetOnConditionAsync(p => p.MeterDataId == meterDataId);

            var paymentModel = _mapper.Map<IEnumerable<PaymentModel>>(paymentData);

            return paymentModel;
        }

        public async Task<double> GetPaidSumAsync(int meterDataId)
        {
            var payments = await GetByMeterDataIdAsync(meterDataId);

            if (payments == null || !payments.Any())
                return 0;

            var sum = payments.Sum(x => x.Amount);

            return sum;
        }

        public async Task<double> GetTotalSumAsync(int meterDataId)
        {
            var meterData = (await _meterDataRepository
                .GetWithIncludeOnConditionAsync(
                    nameof(MeterDataModel.MeterValues),
                    x => x.Id == meterDataId))
                .FirstOrDefault();

            meterData.EnsureExists(nameof(ErrorMessages.IMS40001));
            meterData.MeterValues.EnsureNotEmpty(nameof(ErrorMessages.IMS40001));

            var meter = await _meterService.GetByIdAsync(meterData.MeterId);

            // Получаем список подтарифов с датой изменения меньше даты добав. показаний.
            var subTariffs = await _subTariffService.GetByTariffIdAsync(
                meter.TariffId, meterData.Date);

            double totalSum = default;

            foreach (var item in meterData.MeterValues)
            {
                var tarifSum = subTariffs
                    .Where(x => x.Number == item.Number)
                    .Single();

                totalSum += item.Difference * tarifSum.Price;
            }

            return totalSum;
        }

        public async Task UpdatePaymentStatusAsync(int meterDataId, int paymentState)
        {
            var meterData = await _meterDataRepository.GetAsync(meterDataId);

            meterData.EnsureExists(nameof(ErrorMessages.IMS40001));

            if (meterData.PaymentState == paymentState)
                return;

            meterData.PaymentState = paymentState;

            await _meterDataRepository.UpdateAsync(meterData);
        }
    }
}
