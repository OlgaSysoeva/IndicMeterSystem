﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using IndicMeterSystem.Data.Entities;
using IndicMeterSystem.Data.Interfaces;
using IndicMeterSystem.Services.Interfaces;
using IndicMeterSystem.Services.Models;

namespace IndicMeterSystem.Services.Services
{
    public class TariffTypeService : ITariffTypeService
    {
        private readonly IRepository<TariffType> _repository;
        private readonly IMapper _mapper;

        public TariffTypeService(IRepository<TariffType> tariffTypeRepository, IMapper mapper)
        {
            _repository = tariffTypeRepository;
            _mapper = mapper;
        }

        public async Task<IEnumerable<TariffTypeModel>> GetAllAsync()
        {
            var data = await _repository.GetAllAsync();
            var models = _mapper.Map<IEnumerable<TariffTypeModel>>(data);
            return models;
        }

        public async Task<TariffTypeModel> GetByIdAsync(int tariffTypeId)
        {
            var data = await _repository.GetOnConditionAsync(p => p.Id == tariffTypeId);
            var model = _mapper.Map<TariffTypeModel>(data.FirstOrDefault());
            return model;
        }
    }
}
