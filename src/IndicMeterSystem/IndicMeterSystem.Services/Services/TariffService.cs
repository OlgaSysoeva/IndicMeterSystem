﻿using AutoMapper;
using IndicMeterSystem.Common.Exceptions;
using IndicMeterSystem.Common.Properties;
using IndicMeterSystem.Data.Entities;
using IndicMeterSystem.Data.Interfaces;
using IndicMeterSystem.Services.Interfaces;
using IndicMeterSystem.Services.Models;
using System.Linq;
using System.Threading.Tasks;

namespace IndicMeterSystem.Services.Services
{
    /// <summary>
    /// Сервис для работы с тарифами
    /// </summary>
    public class TariffService : ITariffService
    {
        private readonly IRepository<Tariff> _tariffRepository;
        private readonly IRepository<TariffType> _tariffTypeRepository;
        private readonly ISubTariffService _subTariffService;
        private readonly IMapper _mapper;

        public TariffService(IRepository<Tariff> tariffRepository,
            IRepository<TariffType> tariffTypeRepository,
            ISubTariffService subTariffService,
            IMapper mapper)
        {
            _tariffRepository = tariffRepository;
            _tariffTypeRepository = tariffTypeRepository;
            _subTariffService = subTariffService;
            _mapper = mapper;
        }

        /// <summary>
        /// Получение модели тарифа по Id
        /// </summary>
        public async Task<TariffExtendedModel> GetByIdAsync(int tariffId)
        {
            var tariff = (await _tariffRepository
                .GetWithIncludeOnConditionAsync(
                    nameof(TariffExtendedModel.TariffType),
                    t => t.Id == tariffId))
                .FirstOrDefault();
            tariff.EnsureExists(nameof(ErrorMessages.IMS40001));

            var tariffModel = _mapper.Map<TariffExtendedModel>(tariff);
            //Подтарифы получаем в сервисе подтарифов, чтобы механизм выборки по дате был тольков одном месте
            tariffModel.SubTariffs = (await _subTariffService.GetByTariffIdAsync(tariffId)).ToList();
            return tariffModel;
        }

        /// <summary>
        /// Создание нового тарифа с подтарифами
        /// </summary>
        public async Task AddAsync(TariffExtendedModel tariffModel)
        {
            //Проверяем входящие данные
            if (tariffModel == null)
                throw new ValidationException(nameof(tariffModel), "Аргумент не может быть пустым");
            await ValidateSubTariffs(tariffModel);

            var tariff = _mapper.Map<Tariff>(tariffModel);
            tariff.Id = 0;      //Обнуляем, чтобы присвоился автоматически
            await _tariffRepository.AddAsync(tariff);
        }

        /// <summary>
        /// Удаление тарифа с указанным Id
        /// </summary>
        public async Task DeleteAsync(int tariffId)
        {
            var tariff = await _tariffRepository.GetAsync(tariffId);
            if (tariff == null) throw new NotFoundException("Тариф не найден"); 
            tariff.Deleted = true;
            await _tariffRepository.UpdateAsync(tariff);
        }

        public async Task UpdateAsync(TariffExtendedModel tariffModel)
        {
            //Проверяем входящие данные
            if (tariffModel == null)
                throw new ValidationException(nameof(tariffModel), "Аргумент не может быть пустым");
            //Проверяем наличие тарифа в БД
            var tariff = await _tariffRepository.GetAsync(tariffModel.Id);
            if (tariff == null) throw new NotFoundException("Тариф не найден");

            //Меняем тариф - только два поля: название и единицы. Подтарифы здесь не трогаем
            tariff.Title = tariffModel.Title;
            tariff.Unit = tariffModel.Unit;
            await _tariffRepository.UpdateAsync(tariff);
        }

        //Получаем количество подтарифов по Id типа тарифа
        private async Task<int> GetSubTariffCountByTariffTypeId(int tariffTypeId)
        {
            var tariffType = await _tariffTypeRepository.GetAsync(tariffTypeId);
            tariffType.EnsureExists(nameof(ErrorMessages.IMS40001));
            return tariffType.SubTariffCount;
        }

        //Проверяем валидность списка подтарифов, полученных в модели тарифов
        private async Task ValidateSubTariffs(TariffExtendedModel tariffModel)
        {
            //Список не null и не пустой
            if (tariffModel.SubTariffs == null || tariffModel.SubTariffs.Count == 0)
                throw new ValidationException("SubTariffList", "Список подтарифов не может быть пустым");
            var subTariffCount = await GetSubTariffCountByTariffTypeId(tariffModel.TariffTypeId);
            //Проверяем количество подтарифов
            if (tariffModel.SubTariffs.Count != subTariffCount)
                throw new ValidationException("SubTariffList", $"Должно быть {subTariffCount} подтарифа для данного типа тарифа");
            //Проверяем корректность значений поля Number
            if (tariffModel.SubTariffs.Where(st => st.Number < 1 || st.Number > subTariffCount).Any())
                throw new ValidationException("SubTariffList", $"Допустимые значения поля Number подтарифа [1 - {subTariffCount}]");
            //Проверяем уникальность значений поля Number
            if (tariffModel.SubTariffs.GroupBy(st => st.Number, (_, group) => new { count = group.Count() }).Where(i => i.count > 1).Any())
                throw new ValidationException("SubTariffList", $"Значения поля Number не могут повторяться");
        }
    }
}
