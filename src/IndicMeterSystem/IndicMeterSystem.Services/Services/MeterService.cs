﻿using AutoMapper;
using IndicMeterSystem.Common.Exceptions;
using IndicMeterSystem.Common.Properties;
using IndicMeterSystem.Data.Entities;
using IndicMeterSystem.Data.Interfaces;
using IndicMeterSystem.Services.Interfaces;
using IndicMeterSystem.Services.Models;

using System.Collections.Generic;
using System.Threading.Tasks;

namespace IndicMeterSystem.Services.Services
{
    public class MeterService : IMeterService
    {
        private readonly IRepository<Meter> _meterRepository;
        private readonly IMapper _mapper;

        public MeterService(IRepository<Meter> meterRepository, IMapper mapper)
        {
            _meterRepository = meterRepository;
            _mapper = mapper;
        }

        public async Task<IEnumerable<MeterModel>> GetByAddressIdAsync(int addressId)
        {
            var meter = await _meterRepository.GetOnConditionAsync(m => m.AddressId == addressId && m.Deleted == false);
            var meterModels = _mapper.Map<IEnumerable<MeterModel>>(meter);

            return meterModels;
        }

        public async Task AddMeterAsync(MeterModel meterModel)
        {
            Meter meter = _mapper.Map<Meter>(meterModel);

            await _meterRepository.AddAsync(meter);
        }

        public async Task<MeterModel> GetByIdAsync(int meterId)
        {
            var meter = await _meterRepository.GetAsync(meterId);

            var meterModel = _mapper.Map<MeterModel>(meter);

            return meterModel;
        }

        public async Task<MeterModel> GetCopyAsync(int meterId)
        {
            var meter = await GetByIdAsync(meterId);

            var meterCopy = meter.ShallowCopy();

            return meterCopy;
        }

        public async Task UpdateMeterAsync(MeterModel meterModel)
        {
            //Проверяем наличие счетчика в БД
            var meter = await _meterRepository.GetAsync(meterModel.Id);
            if (meter == null) throw new NotFoundException("Счетчик не найден");

            meter.DateFrom = meterModel.DateFrom;
            meter.DateTo = meterModel.DateTo;
            meter.Description = meterModel.Description;
            meter.SerialNumber = meterModel.SerialNumber;
            meter.TariffId = meterModel.TariffId;
            await _meterRepository.UpdateAsync(meter);
        }

        public async Task DeleteMeterAsync(int meterId)
        {
            var meter = await _meterRepository.GetAsync(meterId);

            meter.EnsureExists(nameof(ErrorMessages.IMS70001));

            if (meter.Deleted)
            {
              throw new ServiceException(nameof(ErrorMessages.IMS70002));
            }

            meter.Deleted = true;
            await _meterRepository.UpdateAsync(meter);
        }


  }
}
