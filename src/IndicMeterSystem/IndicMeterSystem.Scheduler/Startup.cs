﻿using IndicMeterSystem.Data;
using IndicMeterSystem.Data.EF;
using IndicMeterSystem.Data.Interfaces;
using IndicMeterSystem.Data.Repositories;
using IndicMeterSystem.Scheduler.Notifications;
using IndicMeterSystem.Scheduler.Tasks;
using IndicMeterSystem.Scheduler.Сhecks;

using Infrastructure.EventBus.EventBusRabbitMQ;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace IndicMeterSystem.Scheduler
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public virtual void ConfigureServices(IServiceCollection services)
        {
            SemaphoreSetting.MaximumThreadsToAccessDatabase = Configuration.GetValue(
                "MaximumThreadsToAccessDatabase", 3);

            // получаем строку подключения из файла конфигурации
            string connection = Configuration.GetConnectionString("DefaultConnection");

            // добавляем контекст в качестве сервиса в приложение
            services.AddDbContext<ApplicationContext>(options => options.UseNpgsql(connection));

            // добавляем связи интерфейсов с реализацией
            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));

            services.Configure<BackgroundTaskSettings>(this.Configuration)
                    .AddOptions()
                    .AddEventBus(this.Configuration)
                    .AddScoped<UnpaidMeterDataService>()
                    .AddScoped<MissingMeterDataService>()
                    .AddSingleton<NotificationContext>()
                    .AddSingleton<PaymentEmailNotif>()
                    .AddSingleton<PaymentSmsNotif>()
                    .AddSingleton<MeterDataEmailNotif>()
                    .AddSingleton<MeterDataSmsNotif>()
                    .AddScoped<CheckPaymentExist>()
                    .AddScoped<CheckMeterDataExist>()
                    .AddHostedService<CheckPaymentManagerTask>()
                    .AddHostedService<CheckMeterDataManagerTask>();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapGet("/", async context =>
                {
                    await context.Response.WriteAsync("Start Scheduler");
                });
            });
        }
    }
}
