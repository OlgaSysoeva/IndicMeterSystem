﻿using IndicMeterSystem.Scheduler.Interfaces;
using IndicMeterSystem.Scheduler.Notifications;

using Microsoft.Extensions.Logging;

using System;
using System.Threading.Tasks;

namespace IndicMeterSystem.Scheduler.Сhecks
{
    public class CheckMeterDataExist : ICheck
    {
        private readonly ILogger<CheckMeterDataExist> _logger;
        private readonly MissingMeterDataService _missingService;
        private readonly NotificationContext _notificationContext;
        private readonly MeterDataEmailNotif _emailNotification;
        private readonly MeterDataSmsNotif _smsNotification;

        public CheckMeterDataExist(
            ILogger<CheckMeterDataExist> logger,
            MissingMeterDataService missingService,
            NotificationContext notificationContext,
            MeterDataEmailNotif emailNotification,
            MeterDataSmsNotif smsNotification)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _missingService = missingService ?? throw new ArgumentNullException(nameof(missingService));
            _notificationContext = notificationContext ?? throw new ArgumentNullException(nameof(notificationContext));
            _emailNotification = emailNotification ?? throw new ArgumentNullException(nameof(emailNotification));
            _smsNotification = smsNotification ?? throw new ArgumentNullException(nameof(smsNotification));
        }

        public async Task ExecuteAsync()
        {
            _logger.LogDebug("Check for missing meter data.");

            var users = await _missingService.GetMissingMeterDataAsync();

            foreach (var user in users)
            {
                if (user.GetSmsNotification && user.GetEmailNotification)
                {
                    _notificationContext.SetNotification(_smsNotification);
                    _notificationContext.Execute(user);
                    _notificationContext.SetNotification(_emailNotification);
                }
                else if (user.GetEmailNotification)
                {
                    _notificationContext.SetNotification(_emailNotification);
                }
                else if (user.GetSmsNotification)
                {
                    _notificationContext.SetNotification(_smsNotification);
                }

                _notificationContext.Execute(user);
            }
        }
    }
}
