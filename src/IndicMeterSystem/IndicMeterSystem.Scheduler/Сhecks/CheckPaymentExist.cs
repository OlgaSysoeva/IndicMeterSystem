﻿using IndicMeterSystem.Scheduler.Interfaces;
using IndicMeterSystem.Scheduler.Notifications;

using Microsoft.Extensions.Logging;

using System;
using System.Threading.Tasks;

namespace IndicMeterSystem.Scheduler.Сhecks
{
    public class CheckPaymentExist : ICheck
    {
        private readonly ILogger<CheckPaymentExist> _logger;
        private readonly UnpaidMeterDataService _unpaidService;
        private readonly NotificationContext _notificationContext;
        private readonly PaymentEmailNotif _emailNotification;
        private readonly PaymentSmsNotif _smsNotification;

        public CheckPaymentExist(
            ILogger<CheckPaymentExist> logger,
            UnpaidMeterDataService unpaidService,
            NotificationContext notificationContext,
            PaymentEmailNotif emailNotification,
            PaymentSmsNotif smsNotification)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _unpaidService = unpaidService ?? throw new ArgumentNullException(nameof(unpaidService));
            _notificationContext = notificationContext ?? throw new ArgumentNullException(nameof(notificationContext));
            _emailNotification = emailNotification ?? throw new ArgumentNullException(nameof(emailNotification));
            _smsNotification = smsNotification ?? throw new ArgumentNullException(nameof(smsNotification));
        }

        public async Task ExecuteAsync()
        {
            _logger.LogDebug("Check for unpaid meter data.");

            var users = await _unpaidService.GetUnpaidMeterDataAsync();

            foreach (var user in users)
            {
                if (user.GetSmsNotification && user.GetEmailNotification)
                {
                    _notificationContext.SetNotification(_smsNotification);
                    _notificationContext.Execute(user);
                    _notificationContext.SetNotification(_emailNotification);
                }
                else if (user.GetEmailNotification)
                {
                    _notificationContext.SetNotification(_emailNotification);
                }
                else if (user.GetSmsNotification)
                {
                    _notificationContext.SetNotification(_smsNotification);
                }
                
                _notificationContext.Execute(user);
            }
        }
    }
}
