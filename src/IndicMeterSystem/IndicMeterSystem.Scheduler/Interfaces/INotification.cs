﻿namespace IndicMeterSystem.Scheduler.Interfaces
{
    public interface INotification
    {
        /// <summary>
        /// Отправляет сообщения-оповещения в event bus.
        /// </summary>
        /// <param name="data">Модель данных пользователя.</param>
        void SendNotification(UserModel data);
    }
}
