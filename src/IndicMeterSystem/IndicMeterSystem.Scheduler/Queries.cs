﻿namespace IndicMeterSystem.Scheduler
{
    public static class Queries
    {
        public const string GetDataWithoutPayment = @"
SELECT DISTINCT ""Adresses"".""RegistrationAddress"", ""Users"".""Email"", ""MeterDatas"".""Date"",
""Users"".""PhoneNumber"", ""Users"".""GetEmailNotification"", ""Users"".""GetSmsNotification""
FROM ""MeterDatas""
INNER JOIN ""Meters"" ON ""MeterDatas"".""MeterId"" = ""Meters"".""Id"" 
INNER JOIN ""Adresses"" ON ""Meters"".""AddressId"" = ""Adresses"".""Id"" 
INNER JOIN ""Users"" ON ""Adresses"".""UserId"" = ""Users"".""Id"" 
WHERE ""Users"".""Deleted"" <> TRUE 
AND ""Adresses"".""Deleted"" <> TRUE 
AND ""MeterDatas"".""PaymentState"" <> 3
AND ""Meters"".""Deleted"" = FALSE ";

        public const string GetDataWithoutMeterData = @"
SELECT DISTINCT ""Adresses"".""RegistrationAddress"", ""Users"".""Email"",
""Users"".""PhoneNumber"", ""Users"".""GetEmailNotification"", ""Users"".""GetSmsNotification""
FROM ""Meters""
INNER JOIN ""Adresses"" ON ""Meters"".""AddressId"" = ""Adresses"".""Id"" 
INNER JOIN ""Users"" ON ""Adresses"".""UserId"" = ""Users"".""Id"" 
WHERE ""Users"".""Deleted"" <> TRUE 
AND ""Adresses"".""Deleted"" <> TRUE 
AND ""Meters"".""Deleted"" = FALSE
AND ""Meters"".""Id"" NOT IN 
(SELECT DISTINCT ""MeterDatas"".""MeterId"" FROM ""MeterDatas"" 
    WHERE ""MeterDatas"".""Date"" >= @DateStart AND ""MeterDatas"".""Date"" < @DateEnd)";
    }
}
