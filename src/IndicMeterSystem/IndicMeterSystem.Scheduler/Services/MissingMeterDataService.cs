﻿using IndicMeterSystem.Data.Entities;
using IndicMeterSystem.Data.Interfaces;

using Microsoft.Extensions.DependencyInjection;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IndicMeterSystem.Scheduler
{
    /// <summary>
    /// Работает со списком моделей User.
    /// </summary>
    public class MissingMeterDataService
    {
        private readonly IServiceProvider _services;

        public MissingMeterDataService(
            IServiceProvider services)
        {
            _services = services;
        }

        /// <summary>
        /// Получает данные пользов. с отсутствующими показаниями.
        /// </summary>
        /// <returns>Список данных пользователей.</returns>
        public async Task<IEnumerable<UserModel>> GetMissingMeterDataAsync()
        {
            var firstDayOfMonth = 1;

            var dateStart = new DateTime(
                DateTime.Today.Year,
                DateTime.Today.Month,
                firstDayOfMonth);

            var dateEnd = dateStart.AddMonths(1);

            using var scopeMeter = _services.CreateScope();
            using var scopeMeterData = _services.CreateScope();

            var scopedMeterRepository =scopeMeter.ServiceProvider
                        .GetRequiredService<IRepository<Meter>>();
            var scopedMeterDataRepository = scopeMeterData.ServiceProvider
                        .GetRequiredService<IRepository<MeterData>>();

            // Находим имеющиеся идентификаторы в сущности показаний на период.
            var meterIds = (await scopedMeterDataRepository.GetOnConditionAsync(
                    x => x.Date >= dateStart && x.Date < dateEnd))
                .Select(x => x.Id)
                .Distinct();

            var datas = await scopedMeterRepository.GetWithIncludeOnConditionAsync(
                $"{nameof(Meter.Address)}.{nameof(Address.User)}",
                m => m.Deleted == false
                    && m.Address.Deleted == false
                    && m.Address.User.Deleted == false
                    && !meterIds.Contains(m.Id)
                );

            var users = datas?.Select(x => new UserModel
            {
                Address = x.Address.RegistrationAddress,
                Email = x.Address.User.Email,
                PhoneNumber = x.Address.User.PhoneNumber,
                GetEmailNotification = x.Address.User.GetEmailNotification,
                GetSmsNotification = x.Address.User.GetSmsNotification
            })
            .Distinct();

            return users;
        }
    }
}
