﻿using IndicMeterSystem.Data.Entities;
using IndicMeterSystem.Data.Enums;
using IndicMeterSystem.Data.Interfaces;

using Microsoft.Extensions.DependencyInjection;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IndicMeterSystem.Scheduler
{
    /// <summary>
    /// Работает со списком моделей User.
    /// </summary>
    public class UnpaidMeterDataService
    {
        private readonly IServiceProvider _services;

        public UnpaidMeterDataService(
            IServiceProvider services)
        {
            _services = services;
        }

        /// <summary>
        /// Получает данные пользов. с неоплаченными показаниями.
        /// </summary>
        /// <returns>Список данных пользователей.</returns>
        public async Task<IEnumerable<UserModel>> GetUnpaidMeterDataAsync()
        {
            using var scope = _services.CreateScope();
            var scopedRepository =
                scope.ServiceProvider
                    .GetRequiredService<IRepository<MeterData>>();

            var datas = await scopedRepository.GetWithIncludeOnConditionAsync(
                $"{nameof(MeterData.Meter)}.{nameof(Meter.Address)}.{nameof(Address.User)}",
                md => md.Meter.Deleted == false
                    && md.Meter.Address.Deleted == false
                    && md.Meter.Address.User.Deleted == false
                    && md.PaymentState != (int)PaymentStateEnum.Full
                );

            var users = datas?.Select(x => new UserModel
            {
                Address = x.Meter.Address.RegistrationAddress,
                Date = x.Date,
                Email = x.Meter.Address.User.Email,
                PhoneNumber = x.Meter.Address.User.PhoneNumber,
                GetEmailNotification = x.Meter.Address.User.GetEmailNotification,
                GetSmsNotification = x.Meter.Address.User.GetSmsNotification
            })
            .Distinct();

            return users;
        }
    }
}
