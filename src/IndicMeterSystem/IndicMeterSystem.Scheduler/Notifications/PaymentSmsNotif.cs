﻿using BusinesEvent;

using IndicMeterSystem.Scheduler.Interfaces;
using Infrastructure.EventBus.EventBusInfrastructure.Abstraction;

using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

using System;

namespace IndicMeterSystem.Scheduler.Notifications
{
    public class PaymentSmsNotif : INotification
    {
        private readonly ILogger<PaymentSmsNotif> _logger;
        private readonly BackgroundTaskSettings _settings;
        private readonly IEventBus _eventBus;

        public PaymentSmsNotif(
            IOptions<BackgroundTaskSettings> settings,
            ILogger<PaymentSmsNotif> logger,
            IEventBus eventBus)
        {
            _settings = settings?.Value ?? throw new ArgumentNullException(nameof(settings));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _eventBus = eventBus ?? throw new ArgumentNullException(nameof(eventBus));
        }

        public void SendNotification(UserModel data)
        {
            if (data.PhoneNumber == null)
            {
                return;
            }

            using var notificationEvent = new UserPaymentSmsNotifEvent(
                data.PhoneNumber,
                DateTime.Today.AddDays(_settings.EventLifeTimeDays),
                data.Address,
                data.Date);

            _logger.LogDebug("----- Publishing integration event: " +
                "{IntegrationEventId} from {AppName} - ({@IntegrationEvent})",
                notificationEvent.Id, Program.AppName, notificationEvent);

            _eventBus.Publish(notificationEvent);
        }
    }
}
