﻿using BusinesEvent;

using IndicMeterSystem.Scheduler.Interfaces;
using Infrastructure.EventBus.EventBusInfrastructure.Abstraction;

using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

using System;

namespace IndicMeterSystem.Scheduler.Notifications
{
    public class MeterDataEmailNotif : INotification
    {
        private readonly ILogger<MeterDataEmailNotif> _logger;
        private readonly BackgroundTaskSettings _settings;
        private readonly IEventBus _eventBus;

        public MeterDataEmailNotif(
            IOptions<BackgroundTaskSettings> settings,
            ILogger<MeterDataEmailNotif> logger,
            IEventBus eventBus)
        {
            _settings = settings?.Value ?? throw new ArgumentNullException(nameof(settings));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _eventBus = eventBus ?? throw new ArgumentNullException(nameof(eventBus));
        }

        public void SendNotification(UserModel data)
        {
            if (data.Email == null)
            {
                return;
            }

            using var notificationEvent = new UserMeterDataEmailNotifEvent(
                data.Email,
                DateTime.Today.AddDays(_settings.EventLifeTimeDays),
                data.Address);

            _logger.LogDebug("----- Publishing integration event: " +
                "{IntegrationEventId} from {AppName} - ({@IntegrationEvent})",
                notificationEvent.Id, Program.AppName, notificationEvent);

            _eventBus.Publish(notificationEvent);
        }
    }
}
