﻿using IndicMeterSystem.Scheduler.Interfaces;

namespace IndicMeterSystem.Scheduler.Notifications
{
    public class NotificationContext
    {
        private INotification _notification;

        /// <summary>
        /// Устанавливает объект оповещения.
        /// </summary>
        /// <param name="notification">Объект оповещения.</param>
        public void SetNotification(INotification notification)
        {
            _notification = notification;
        }

        /// <summary>
        /// Запуск выполнения отправки оповещений.
        /// </summary>
        /// <param name="data">Модель данных пользователей.</param>
        public void Execute(UserModel data)
        {
            _notification.SendNotification(data);
        }
    }
}
