﻿using IndicMeterSystem.Scheduler.Сhecks;

using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

using System;
using System.Threading;
using System.Threading.Tasks;

namespace IndicMeterSystem.Scheduler.Tasks
{
    public class CheckMeterDataManagerTask : BackgroundService
    {
        private readonly ILogger<CheckMeterDataManagerTask> _logger;
        private readonly BackgroundTaskSettings _settings;
        private readonly CheckMeterDataExist _checkMeterDataExist;

        public CheckMeterDataManagerTask(
            IOptions<BackgroundTaskSettings> settings,
            ILogger<CheckMeterDataManagerTask> logger,
            CheckMeterDataExist checkMeterDataExist)
        {
            _settings = settings?.Value ?? throw new ArgumentNullException(nameof(settings));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _checkMeterDataExist = checkMeterDataExist ?? throw new ArgumentNullException(nameof(checkMeterDataExist));
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _logger.LogDebug("CheckMeterDataManagerTask is starting.");

            stoppingToken.Register(() => _logger.LogDebug(
                "#1 CheckMeterDataManagerTask background task is stopping."));

            while (!stoppingToken.IsCancellationRequested)
            {
                _logger.LogDebug(
                    "CheckMeterDataManagerTask background task is doing background work.");

                // Проверяем дату на вхождение в период проверки.
                if (DateTime.Now.Day >= _settings.MonthDayCheckMeterDataStart
                    && DateTime.Now.Day <= _settings.MonthDayCheckMeterDataEnd)
                {
                    await _checkMeterDataExist.ExecuteAsync();
                }

                await Task.Delay(_settings.TaskRunInterval, stoppingToken);
            }

            _logger.LogDebug("CheckMeterDataManagerTask background task is stopping.");

            await Task.CompletedTask;
        }
    }
}
